﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '   <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    ' <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ForceItemsIntoToolBox1 = New MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox()
        Me.ModbusTCPCom1 = New AdvancedHMIDrivers.ModbusTCPCom(Me.components)
        Me.HydraulicCylinder1 = New AdvancedHMIControls.HydraulicCylinder()
        Me.HydraulicCylinder2 = New AdvancedHMIControls.HydraulicCylinder()
        Me.HydraulicCylinder3 = New AdvancedHMIControls.HydraulicCylinder()
        Me.WaterPump1 = New AdvancedHMIControls.WaterPump()
        Me.Tank1 = New AdvancedHMIControls.Tank()
        Me.Tank2 = New AdvancedHMIControls.Tank()
        Me.Tank3 = New AdvancedHMIControls.Tank()
        Me.SelectorSwitch1 = New AdvancedHMIControls.SelectorSwitch()
        Me.SelectorSwitch2 = New AdvancedHMIControls.SelectorSwitch()
        Me.SelectorSwitch3 = New AdvancedHMIControls.SelectorSwitch()
        Me.GraphicIndicator1 = New AdvancedHMIControls.GraphicIndicator()
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'ModbusTCPCom1
        '
        Me.ModbusTCPCom1.DisableSubscriptions = false
        Me.ModbusTCPCom1.IniFileName = ""
        Me.ModbusTCPCom1.IniFileSection = Nothing
        Me.ModbusTCPCom1.IPAddress = "127.0.0.1"
        Me.ModbusTCPCom1.MaxReadGroupSize = 20
        Me.ModbusTCPCom1.SwapBytes = true
        Me.ModbusTCPCom1.SwapWords = false
        Me.ModbusTCPCom1.TcpipPort = CType(502US,UShort)
        Me.ModbusTCPCom1.TimeOut = 3000
        Me.ModbusTCPCom1.UnitId = CType(1,Byte)
        '
        'HydraulicCylinder1
        '
        Me.HydraulicCylinder1.BackColor = System.Drawing.Color.Beige
        Me.HydraulicCylinder1.ComComponent = Me.ModbusTCPCom1
        Me.HydraulicCylinder1.ForeColor = System.Drawing.Color.LightGray
        Me.HydraulicCylinder1.Location = New System.Drawing.Point(592, 44)
        Me.HydraulicCylinder1.Name = "HydraulicCylinder1"
        Me.HydraulicCylinder1.OutputType = AdvancedHMIControls.HydraulicCylinder.OutputTypes.MomentarySet
        Me.HydraulicCylinder1.PLCAddressClick = ""
        Me.HydraulicCylinder1.PLCAddressText = ""
        Me.HydraulicCylinder1.PLCAddressValue = "40011"
        Me.HydraulicCylinder1.PLCAddressVisible = ""
        Me.HydraulicCylinder1.Rotation = System.Drawing.RotateFlipType.RotateNoneFlipNone
        Me.HydraulicCylinder1.Size = New System.Drawing.Size(159, 87)
        Me.HydraulicCylinder1.TabIndex = 39
        Me.HydraulicCylinder1.Text = "Pump 1"
        Me.HydraulicCylinder1.Value = false
        '
        'HydraulicCylinder2
        '
        Me.HydraulicCylinder2.ComComponent = Me.ModbusTCPCom1
        Me.HydraulicCylinder2.ForeColor = System.Drawing.Color.LightGray
        Me.HydraulicCylinder2.Location = New System.Drawing.Point(592, 148)
        Me.HydraulicCylinder2.Name = "HydraulicCylinder2"
        Me.HydraulicCylinder2.OutputType = AdvancedHMIControls.HydraulicCylinder.OutputTypes.MomentarySet
        Me.HydraulicCylinder2.PLCAddressClick = ""
        Me.HydraulicCylinder2.PLCAddressText = ""
        Me.HydraulicCylinder2.PLCAddressValue = "40012"
        Me.HydraulicCylinder2.PLCAddressVisible = ""
        Me.HydraulicCylinder2.Rotation = System.Drawing.RotateFlipType.RotateNoneFlipNone
        Me.HydraulicCylinder2.Size = New System.Drawing.Size(159, 87)
        Me.HydraulicCylinder2.TabIndex = 40
        Me.HydraulicCylinder2.Text = "Pump 2"
        Me.HydraulicCylinder2.Value = false
        '
        'HydraulicCylinder3
        '
        Me.HydraulicCylinder3.ComComponent = Me.ModbusTCPCom1
        Me.HydraulicCylinder3.ForeColor = System.Drawing.Color.LightGray
        Me.HydraulicCylinder3.Location = New System.Drawing.Point(592, 241)
        Me.HydraulicCylinder3.Name = "HydraulicCylinder3"
        Me.HydraulicCylinder3.OutputType = AdvancedHMIControls.HydraulicCylinder.OutputTypes.MomentarySet
        Me.HydraulicCylinder3.PLCAddressClick = ""
        Me.HydraulicCylinder3.PLCAddressText = ""
        Me.HydraulicCylinder3.PLCAddressValue = "40013"
        Me.HydraulicCylinder3.PLCAddressVisible = ""
        Me.HydraulicCylinder3.Rotation = System.Drawing.RotateFlipType.RotateNoneFlipNone
        Me.HydraulicCylinder3.Size = New System.Drawing.Size(159, 87)
        Me.HydraulicCylinder3.TabIndex = 41
        Me.HydraulicCylinder3.Text = "Pump 3"
        Me.HydraulicCylinder3.Value = false
        '
        'WaterPump1
        '
        Me.WaterPump1.ComComponent = Me.ModbusTCPCom1
        Me.WaterPump1.Location = New System.Drawing.Point(38, 66)
        Me.WaterPump1.Name = "WaterPump1"
        Me.WaterPump1.PLCAddressText = ""
        Me.WaterPump1.PLCAddressValue = "40011"
        Me.WaterPump1.PLCAddressVisible = ""
        Me.WaterPump1.Size = New System.Drawing.Size(208, 103)
        Me.WaterPump1.TabIndex = 43
        Me.WaterPump1.Value = false
        '
        'Tank1
        '
        Me.Tank1.ComComponent = Me.ModbusTCPCom1
        Me.Tank1.ForeColor = System.Drawing.Color.White
        Me.Tank1.HighlightColor = System.Drawing.Color.Red
        Me.Tank1.HighlightKeyCharacter = "!"
        Me.Tank1.KeypadText = Nothing
        Me.Tank1.Location = New System.Drawing.Point(541, 28)
        Me.Tank1.MaxValue = 10
        Me.Tank1.MinValue = 0
        Me.Tank1.Name = "Tank1"
        Me.Tank1.NumericFormat = Nothing
        Me.Tank1.PLCAddressKeypad = ""
        Me.Tank1.PLCAddressText = ""
        Me.Tank1.PLCAddressValue = "40011"
        Me.Tank1.PLCAddressVisible = ""
        Me.Tank1.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.Tank1.Size = New System.Drawing.Size(55, 88)
        Me.Tank1.TabIndex = 47
        Me.Tank1.TankContentColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.Tank1.TextPrefix = Nothing
        Me.Tank1.TextSuffix = Nothing
        Me.Tank1.Value = 0!
        Me.Tank1.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Tank2
        '
        Me.Tank2.ComComponent = Me.ModbusTCPCom1
        Me.Tank2.ForeColor = System.Drawing.Color.White
        Me.Tank2.HighlightColor = System.Drawing.Color.Red
        Me.Tank2.HighlightKeyCharacter = "!"
        Me.Tank2.KeypadText = Nothing
        Me.Tank2.Location = New System.Drawing.Point(541, 122)
        Me.Tank2.MaxValue = 10
        Me.Tank2.MinValue = 0
        Me.Tank2.Name = "Tank2"
        Me.Tank2.NumericFormat = Nothing
        Me.Tank2.PLCAddressKeypad = ""
        Me.Tank2.PLCAddressText = ""
        Me.Tank2.PLCAddressValue = "40012"
        Me.Tank2.PLCAddressVisible = ""
        Me.Tank2.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.Tank2.Size = New System.Drawing.Size(55, 88)
        Me.Tank2.TabIndex = 48
        Me.Tank2.TankContentColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.Tank2.TextPrefix = Nothing
        Me.Tank2.TextSuffix = Nothing
        Me.Tank2.Value = 0!
        Me.Tank2.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Tank3
        '
        Me.Tank3.ComComponent = Me.ModbusTCPCom1
        Me.Tank3.ForeColor = System.Drawing.Color.White
        Me.Tank3.HighlightColor = System.Drawing.Color.Red
        Me.Tank3.HighlightKeyCharacter = "!"
        Me.Tank3.KeypadText = Nothing
        Me.Tank3.Location = New System.Drawing.Point(541, 216)
        Me.Tank3.MaxValue = 10
        Me.Tank3.MinValue = 0
        Me.Tank3.Name = "Tank3"
        Me.Tank3.NumericFormat = Nothing
        Me.Tank3.PLCAddressKeypad = ""
        Me.Tank3.PLCAddressText = ""
        Me.Tank3.PLCAddressValue = "40013"
        Me.Tank3.PLCAddressVisible = ""
        Me.Tank3.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.Tank3.Size = New System.Drawing.Size(55, 88)
        Me.Tank3.TabIndex = 49
        Me.Tank3.TankContentColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.Tank3.TextPrefix = Nothing
        Me.Tank3.TextSuffix = Nothing
        Me.Tank3.Value = 0!
        Me.Tank3.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'SelectorSwitch1
        '
        Me.SelectorSwitch1.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch1.ForeColor = System.Drawing.Color.Black
        Me.SelectorSwitch1.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch1.Location = New System.Drawing.Point(448, 28)
        Me.SelectorSwitch1.Name = "SelectorSwitch1"
        Me.SelectorSwitch1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.SelectorSwitch1.PLCAddressClick = "40011"
        Me.SelectorSwitch1.PLCAddressText = ""
        Me.SelectorSwitch1.PLCAddressValue = ""
        Me.SelectorSwitch1.PLCAddressVisible = ""
        Me.SelectorSwitch1.Size = New System.Drawing.Size(52, 76)
        Me.SelectorSwitch1.TabIndex = 50
        Me.SelectorSwitch1.Text = "Valve 1"
        Me.SelectorSwitch1.Value = false
        '
        'SelectorSwitch2
        '
        Me.SelectorSwitch2.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch2.ForeColor = System.Drawing.Color.Black
        Me.SelectorSwitch2.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch2.Location = New System.Drawing.Point(448, 122)
        Me.SelectorSwitch2.Name = "SelectorSwitch2"
        Me.SelectorSwitch2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.SelectorSwitch2.PLCAddressClick = "40012"
        Me.SelectorSwitch2.PLCAddressText = ""
        Me.SelectorSwitch2.PLCAddressValue = ""
        Me.SelectorSwitch2.PLCAddressVisible = ""
        Me.SelectorSwitch2.Size = New System.Drawing.Size(52, 76)
        Me.SelectorSwitch2.TabIndex = 51
        Me.SelectorSwitch2.Text = "Valve 2"
        Me.SelectorSwitch2.Value = false
        '
        'SelectorSwitch3
        '
        Me.SelectorSwitch3.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3.ForeColor = System.Drawing.Color.Black
        Me.SelectorSwitch3.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch3.Location = New System.Drawing.Point(448, 216)
        Me.SelectorSwitch3.Name = "SelectorSwitch3"
        Me.SelectorSwitch3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.SelectorSwitch3.PLCAddressClick = "40013"
        Me.SelectorSwitch3.PLCAddressText = ""
        Me.SelectorSwitch3.PLCAddressValue = ""
        Me.SelectorSwitch3.PLCAddressVisible = ""
        Me.SelectorSwitch3.Size = New System.Drawing.Size(52, 76)
        Me.SelectorSwitch3.TabIndex = 52
        Me.SelectorSwitch3.Text = "Valve 3"
        Me.SelectorSwitch3.Value = false
        '
        'GraphicIndicator1
        '
        Me.GraphicIndicator1.ComComponent = Me.ModbusTCPCom1
        Me.GraphicIndicator1.Font2 = New System.Drawing.Font("Arial", 10!)
        Me.GraphicIndicator1.GraphicAllOff = Nothing
        Me.GraphicIndicator1.GraphicSelect1 = Nothing
        Me.GraphicIndicator1.GraphicSelect2 = Nothing
        Me.GraphicIndicator1.Location = New System.Drawing.Point(38, 12)
        Me.GraphicIndicator1.Name = "GraphicIndicator1"
        Me.GraphicIndicator1.NumericFormat = Nothing
        Me.GraphicIndicator1.OutputType = MfgControl.AdvancedHMI.Controls.GraphicIndicator.OutputTypes.MomentarySet
        Me.GraphicIndicator1.PLCAddressClick = ""
        Me.GraphicIndicator1.PLCAddressText2 = ""
        Me.GraphicIndicator1.PLCAddressValueSelect1 = ""
        Me.GraphicIndicator1.PLCAddressValueSelect2 = ""
        Me.GraphicIndicator1.PLCAddressVisible = ""
        Me.GraphicIndicator1.RotationAngle = MfgControl.AdvancedHMI.Controls.GraphicIndicator.RotationAngleEnum.NoRotation
        Me.GraphicIndicator1.Size = New System.Drawing.Size(208, 48)
        Me.GraphicIndicator1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.GraphicIndicator1.TabIndex = 53
        Me.GraphicIndicator1.Text = "Pleasant Grove Water Co"
        Me.GraphicIndicator1.Text2 = ""
        Me.GraphicIndicator1.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.GraphicIndicator1.ValueSelect1 = false
        Me.GraphicIndicator1.ValueSelect2 = false
        '
        'MainForm
        '
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.GraphicIndicator1)
        Me.Controls.Add(Me.SelectorSwitch3)
        Me.Controls.Add(Me.SelectorSwitch2)
        Me.Controls.Add(Me.SelectorSwitch1)
        Me.Controls.Add(Me.Tank3)
        Me.Controls.Add(Me.Tank2)
        Me.Controls.Add(Me.Tank1)
        Me.Controls.Add(Me.WaterPump1)
        Me.Controls.Add(Me.HydraulicCylinder3)
        Me.Controls.Add(Me.HydraulicCylinder2)
        Me.Controls.Add(Me.HydraulicCylinder1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.KeyPreview = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "AdvancedHMI v3.99n"
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents DF1ComWF1 As AdvancedHMIDrivers.SerialDF1forSLCMicroCom
    Friend WithEvents ForceItemsIntoToolBox1 As MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox
    Friend WithEvents ModbusTCPCom1 As AdvancedHMIDrivers.ModbusTCPCom
    Friend WithEvents HydraulicCylinder1 As AdvancedHMIControls.HydraulicCylinder
    Friend WithEvents HydraulicCylinder2 As AdvancedHMIControls.HydraulicCylinder
    Friend WithEvents HydraulicCylinder3 As AdvancedHMIControls.HydraulicCylinder
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents WaterPump1 As AdvancedHMIControls.WaterPump
    Friend WithEvents Tank1 As AdvancedHMIControls.Tank
    Friend WithEvents Tank2 As AdvancedHMIControls.Tank
    Friend WithEvents Tank3 As AdvancedHMIControls.Tank
    Friend WithEvents SelectorSwitch1 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents SelectorSwitch2 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents SelectorSwitch3 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents GraphicIndicator1 As AdvancedHMIControls.GraphicIndicator
End Class
