﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '   <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    ' <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ForceItemsIntoToolBox1 = New MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox()
        Me.GroupPanel14 = New AdvancedHMIControls.GroupPanel()
        Me.ModbusTCPCom1 = New AdvancedHMIDrivers.ModbusTCPCom(Me.components)
        Me.DateTimeDisplay1 = New AdvancedHMIControls.DateTimeDisplay()
        Me.BasicLabel14 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel10 = New AdvancedHMIControls.GroupPanel()
        Me.GroupPanel13 = New AdvancedHMIControls.GroupPanel()
        Me.BasicButton9 = New AdvancedHMIControls.BasicButton()
        Me.BasicButton10 = New AdvancedHMIControls.BasicButton()
        Me.BasicLabel13 = New AdvancedHMIControls.BasicLabel()
        Me.TempController3 = New AdvancedHMIControls.TempController()
        Me.GroupPanel12 = New AdvancedHMIControls.GroupPanel()
        Me.BasicButton7 = New AdvancedHMIControls.BasicButton()
        Me.BasicButton8 = New AdvancedHMIControls.BasicButton()
        Me.BasicLabel12 = New AdvancedHMIControls.BasicLabel()
        Me.TempController2 = New AdvancedHMIControls.TempController()
        Me.GroupPanel11 = New AdvancedHMIControls.GroupPanel()
        Me.BasicButton5 = New AdvancedHMIControls.BasicButton()
        Me.BasicButton6 = New AdvancedHMIControls.BasicButton()
        Me.BasicLabel8 = New AdvancedHMIControls.BasicLabel()
        Me.TempController1 = New AdvancedHMIControls.TempController()
        Me.BasicLabel11 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel3 = New AdvancedHMIControls.GroupPanel()
        Me.MultiSetButton4 = New AdvancedHMIControls.MultiSetButton()
        Me.MultiSetButton3 = New AdvancedHMIControls.MultiSetButton()
        Me.MultiSetButton2 = New AdvancedHMIControls.MultiSetButton()
        Me.MultiSetButton1 = New AdvancedHMIControls.MultiSetButton()
        Me.GroupPanel8 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState5 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel9 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel5 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState3 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel5 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel9 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState6 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel10 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel7 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState4 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel7 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel6 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState2 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel6 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel4 = New AdvancedHMIControls.GroupPanel()
        Me.AlarmState1 = New AdvancedHMIControls.AlarmState()
        Me.BasicLabel2 = New AdvancedHMIControls.BasicLabel()
        Me.BasicLabel1 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel2 = New AdvancedHMIControls.GroupPanel()
        Me.MultiSetButton5 = New AdvancedHMIControls.MultiSetButton()
        Me.MomentaryButton15 = New AdvancedHMIControls.MomentaryButton()
        Me.MomentaryButton14 = New AdvancedHMIControls.MomentaryButton()
        Me.MomentaryButton13 = New AdvancedHMIControls.MomentaryButton()
        Me.Meter23 = New AdvancedHMIControls.Meter2()
        Me.Meter22 = New AdvancedHMIControls.Meter2()
        Me.Meter21 = New AdvancedHMIControls.Meter2()
        Me.BasicLabel4 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel1 = New AdvancedHMIControls.GroupPanel()
        Me.PilotLight11 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight12 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight9 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight10 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight7 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight8 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight5 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight6 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight4 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight3 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight2 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight1 = New AdvancedHMIControls.PilotLight()
        Me.BasicLabel3 = New AdvancedHMIControls.BasicLabel()
        Me.GroupPanel14.SuspendLayout
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupPanel10.SuspendLayout
        Me.GroupPanel13.SuspendLayout
        Me.GroupPanel12.SuspendLayout
        Me.GroupPanel11.SuspendLayout
        Me.GroupPanel3.SuspendLayout
        Me.GroupPanel8.SuspendLayout
        Me.GroupPanel5.SuspendLayout
        Me.GroupPanel9.SuspendLayout
        Me.GroupPanel7.SuspendLayout
        Me.GroupPanel6.SuspendLayout
        Me.GroupPanel4.SuspendLayout
        Me.GroupPanel2.SuspendLayout
        Me.GroupPanel1.SuspendLayout
        Me.SuspendLayout
        '
        'GroupPanel14
        '
        Me.GroupPanel14.BackColor = System.Drawing.Color.Black
        Me.GroupPanel14.BackColor2 = System.Drawing.Color.Black
        Me.GroupPanel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupPanel14.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel14.Controls.Add(Me.DateTimeDisplay1)
        Me.GroupPanel14.Controls.Add(Me.BasicLabel14)
        Me.GroupPanel14.Location = New System.Drawing.Point(60, 25)
        Me.GroupPanel14.Name = "GroupPanel14"
        Me.GroupPanel14.SelectBackColor2 = false
        Me.GroupPanel14.Size = New System.Drawing.Size(888, 53)
        Me.GroupPanel14.TabIndex = 44
        '
        'ModbusTCPCom1
        '
        Me.ModbusTCPCom1.DisableSubscriptions = false
        Me.ModbusTCPCom1.IniFileName = ""
        Me.ModbusTCPCom1.IniFileSection = Nothing
        Me.ModbusTCPCom1.IPAddress = "10.7.6.130"
        Me.ModbusTCPCom1.MaxReadGroupSize = 20
        Me.ModbusTCPCom1.SwapBytes = true
        Me.ModbusTCPCom1.SwapWords = false
        Me.ModbusTCPCom1.TcpipPort = CType(502US,UShort)
        Me.ModbusTCPCom1.TimeOut = 3000
        Me.ModbusTCPCom1.UnitId = CType(1,Byte)
        '
        'DateTimeDisplay1
        '
        Me.DateTimeDisplay1.AutoSize = true
        Me.DateTimeDisplay1.DisplayFormat = "MM/dd/yyyy hh:mm:ss"
        Me.DateTimeDisplay1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.DateTimeDisplay1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.DateTimeDisplay1.Location = New System.Drawing.Point(691, 16)
        Me.DateTimeDisplay1.Name = "DateTimeDisplay1"
        Me.DateTimeDisplay1.Size = New System.Drawing.Size(174, 20)
        Me.DateTimeDisplay1.TabIndex = 4
        Me.DateTimeDisplay1.Text = "12/07/2016 10:57:37"
        '
        'BasicLabel14
        '
        Me.BasicLabel14.AutoSize = true
        Me.BasicLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel14.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel14.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel14.DisplayAsTime = false
        Me.BasicLabel14.Font = New System.Drawing.Font("Microsoft Sans Serif", 16!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel14.ForeColor = System.Drawing.Color.White
        Me.BasicLabel14.Highlight = false
        Me.BasicLabel14.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel14.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel14.HighlightKeyCharacter = "!"
        Me.BasicLabel14.KeypadAlpahNumeric = false
        Me.BasicLabel14.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel14.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel14.KeypadMaxValue = 0R
        Me.BasicLabel14.KeypadMinValue = 0R
        Me.BasicLabel14.KeypadScaleFactor = 1R
        Me.BasicLabel14.KeypadShowCurrentValue = false
        Me.BasicLabel14.KeypadText = Nothing
        Me.BasicLabel14.KeypadWidth = 300
        Me.BasicLabel14.Location = New System.Drawing.Point(13, 12)
        Me.BasicLabel14.Name = "BasicLabel14"
        Me.BasicLabel14.NumericFormat = Nothing
        Me.BasicLabel14.PLCAddressKeypad = ""
        Me.BasicLabel14.PollRate = 0
        Me.BasicLabel14.Size = New System.Drawing.Size(460, 26)
        Me.BasicLabel14.TabIndex = 3
        Me.BasicLabel14.Text = "Pleasant Grove University Control System"
        Me.BasicLabel14.Value = "Pleasant Grove University Control System"
        Me.BasicLabel14.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel14.ValueLeftPadLength = 0
        Me.BasicLabel14.ValuePrefix = Nothing
        Me.BasicLabel14.ValueScaleFactor = 1R
        Me.BasicLabel14.ValueSuffix = Nothing
        '
        'GroupPanel10
        '
        Me.GroupPanel10.BackColor = System.Drawing.Color.SteelBlue
        Me.GroupPanel10.BackColor2 = System.Drawing.Color.SteelBlue
        Me.GroupPanel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupPanel10.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel10.Controls.Add(Me.GroupPanel13)
        Me.GroupPanel10.Controls.Add(Me.GroupPanel12)
        Me.GroupPanel10.Controls.Add(Me.GroupPanel11)
        Me.GroupPanel10.Controls.Add(Me.BasicLabel11)
        Me.GroupPanel10.Location = New System.Drawing.Point(650, 97)
        Me.GroupPanel10.Name = "GroupPanel10"
        Me.GroupPanel10.SelectBackColor2 = false
        Me.GroupPanel10.Size = New System.Drawing.Size(298, 610)
        Me.GroupPanel10.TabIndex = 43
        '
        'GroupPanel13
        '
        Me.GroupPanel13.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel13.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel13.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel13.Controls.Add(Me.BasicButton9)
        Me.GroupPanel13.Controls.Add(Me.BasicButton10)
        Me.GroupPanel13.Controls.Add(Me.BasicLabel13)
        Me.GroupPanel13.Controls.Add(Me.TempController3)
        Me.GroupPanel13.Location = New System.Drawing.Point(18, 420)
        Me.GroupPanel13.Name = "GroupPanel13"
        Me.GroupPanel13.SelectBackColor2 = false
        Me.GroupPanel13.Size = New System.Drawing.Size(257, 172)
        Me.GroupPanel13.TabIndex = 19
        '
        'BasicButton9
        '
        Me.BasicButton9.BackColor = System.Drawing.Color.Black
        Me.BasicButton9.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton9.Highlight = true
        Me.BasicButton9.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton9.Location = New System.Drawing.Point(8, 109)
        Me.BasicButton9.MaximumHoldTime = 3000
        Me.BasicButton9.MinimumHoldTime = 500
        Me.BasicButton9.Name = "BasicButton9"
        Me.BasicButton9.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton9.PLCAddressClick = "40027"
        Me.BasicButton9.SelectTextAlternate = false
        Me.BasicButton9.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton9.TabIndex = 17
        Me.BasicButton9.Text = "On"
        Me.BasicButton9.TextAlternate = Nothing
        Me.BasicButton9.UseVisualStyleBackColor = false
        Me.BasicButton9.ValueToWrite = 70
        '
        'BasicButton10
        '
        Me.BasicButton10.BackColor = System.Drawing.Color.Black
        Me.BasicButton10.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton10.Highlight = true
        Me.BasicButton10.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton10.Location = New System.Drawing.Point(8, 138)
        Me.BasicButton10.MaximumHoldTime = 3000
        Me.BasicButton10.MinimumHoldTime = 500
        Me.BasicButton10.Name = "BasicButton10"
        Me.BasicButton10.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton10.PLCAddressClick = "40027"
        Me.BasicButton10.SelectTextAlternate = false
        Me.BasicButton10.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton10.TabIndex = 16
        Me.BasicButton10.Text = "Off"
        Me.BasicButton10.TextAlternate = Nothing
        Me.BasicButton10.UseVisualStyleBackColor = false
        Me.BasicButton10.ValueToWrite = 0
        '
        'BasicLabel13
        '
        Me.BasicLabel13.AutoSize = true
        Me.BasicLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel13.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel13.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel13.DisplayAsTime = false
        Me.BasicLabel13.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel13.Highlight = false
        Me.BasicLabel13.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel13.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel13.HighlightKeyCharacter = "!"
        Me.BasicLabel13.KeypadAlpahNumeric = false
        Me.BasicLabel13.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel13.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel13.KeypadMaxValue = 0R
        Me.BasicLabel13.KeypadMinValue = 0R
        Me.BasicLabel13.KeypadScaleFactor = 1R
        Me.BasicLabel13.KeypadShowCurrentValue = false
        Me.BasicLabel13.KeypadText = Nothing
        Me.BasicLabel13.KeypadWidth = 300
        Me.BasicLabel13.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel13.Name = "BasicLabel13"
        Me.BasicLabel13.NumericFormat = Nothing
        Me.BasicLabel13.PLCAddressKeypad = ""
        Me.BasicLabel13.PollRate = 0
        Me.BasicLabel13.Size = New System.Drawing.Size(68, 13)
        Me.BasicLabel13.TabIndex = 0
        Me.BasicLabel13.Text = "Third Floor"
        Me.BasicLabel13.Value = "Third Floor"
        Me.BasicLabel13.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel13.ValueLeftPadLength = 0
        Me.BasicLabel13.ValuePrefix = Nothing
        Me.BasicLabel13.ValueScaleFactor = 1R
        Me.BasicLabel13.ValueSuffix = Nothing
        '
        'TempController3
        '
        Me.TempController3.Button1Text = Nothing
        Me.TempController3.Button2Text = Nothing
        Me.TempController3.ComComponent = Me.ModbusTCPCom1
        Me.TempController3.DecimalPosition = 0
        Me.TempController3.ForeColor = System.Drawing.Color.LightGray
        Me.TempController3.Location = New System.Drawing.Point(104, 23)
        Me.TempController3.Name = "TempController3"
        Me.TempController3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.TempController3.PLCAddressClick1 = "40026"
        Me.TempController3.PLCAddressClick2 = "40027"
        Me.TempController3.PLCAddressClick3 = "40027"
        Me.TempController3.PLCAddressClick4 = "40027"
        Me.TempController3.PLCAddressText = ""
        Me.TempController3.PLCAddressValuePV = "40026"
        Me.TempController3.PLCAddressValueSP = "40027"
        Me.TempController3.PLCAddressVisible = ""
        Me.TempController3.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController3.Size = New System.Drawing.Size(137, 138)
        Me.TempController3.TabIndex = 7
        Me.TempController3.Value_ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController3.Value_ValueScaleFactorSP = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController3.Value2Text = "SP"
        Me.TempController3.ValuePV = 0!
        Me.TempController3.ValueSP = 0!
        '
        'GroupPanel12
        '
        Me.GroupPanel12.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel12.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel12.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel12.Controls.Add(Me.BasicButton7)
        Me.GroupPanel12.Controls.Add(Me.BasicButton8)
        Me.GroupPanel12.Controls.Add(Me.BasicLabel12)
        Me.GroupPanel12.Controls.Add(Me.TempController2)
        Me.GroupPanel12.Location = New System.Drawing.Point(18, 235)
        Me.GroupPanel12.Name = "GroupPanel12"
        Me.GroupPanel12.SelectBackColor2 = false
        Me.GroupPanel12.Size = New System.Drawing.Size(257, 172)
        Me.GroupPanel12.TabIndex = 18
        '
        'BasicButton7
        '
        Me.BasicButton7.BackColor = System.Drawing.Color.Black
        Me.BasicButton7.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton7.Highlight = true
        Me.BasicButton7.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton7.Location = New System.Drawing.Point(8, 109)
        Me.BasicButton7.MaximumHoldTime = 3000
        Me.BasicButton7.MinimumHoldTime = 500
        Me.BasicButton7.Name = "BasicButton7"
        Me.BasicButton7.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton7.PLCAddressClick = "40025"
        Me.BasicButton7.SelectTextAlternate = false
        Me.BasicButton7.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton7.TabIndex = 17
        Me.BasicButton7.Text = "On"
        Me.BasicButton7.TextAlternate = Nothing
        Me.BasicButton7.UseVisualStyleBackColor = false
        Me.BasicButton7.ValueToWrite = 70
        '
        'BasicButton8
        '
        Me.BasicButton8.BackColor = System.Drawing.Color.Black
        Me.BasicButton8.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton8.Highlight = true
        Me.BasicButton8.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton8.Location = New System.Drawing.Point(8, 138)
        Me.BasicButton8.MaximumHoldTime = 3000
        Me.BasicButton8.MinimumHoldTime = 500
        Me.BasicButton8.Name = "BasicButton8"
        Me.BasicButton8.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton8.PLCAddressClick = "40025"
        Me.BasicButton8.SelectTextAlternate = false
        Me.BasicButton8.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton8.TabIndex = 16
        Me.BasicButton8.Text = "Off"
        Me.BasicButton8.TextAlternate = Nothing
        Me.BasicButton8.UseVisualStyleBackColor = false
        Me.BasicButton8.ValueToWrite = 0
        '
        'BasicLabel12
        '
        Me.BasicLabel12.AutoSize = true
        Me.BasicLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel12.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel12.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel12.DisplayAsTime = false
        Me.BasicLabel12.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel12.Highlight = false
        Me.BasicLabel12.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel12.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel12.HighlightKeyCharacter = "!"
        Me.BasicLabel12.KeypadAlpahNumeric = false
        Me.BasicLabel12.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel12.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel12.KeypadMaxValue = 0R
        Me.BasicLabel12.KeypadMinValue = 0R
        Me.BasicLabel12.KeypadScaleFactor = 1R
        Me.BasicLabel12.KeypadShowCurrentValue = false
        Me.BasicLabel12.KeypadText = Nothing
        Me.BasicLabel12.KeypadWidth = 300
        Me.BasicLabel12.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel12.Name = "BasicLabel12"
        Me.BasicLabel12.NumericFormat = Nothing
        Me.BasicLabel12.PLCAddressKeypad = ""
        Me.BasicLabel12.PollRate = 0
        Me.BasicLabel12.Size = New System.Drawing.Size(82, 13)
        Me.BasicLabel12.TabIndex = 0
        Me.BasicLabel12.Text = "Second Floor"
        Me.BasicLabel12.Value = "Second Floor"
        Me.BasicLabel12.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel12.ValueLeftPadLength = 0
        Me.BasicLabel12.ValuePrefix = Nothing
        Me.BasicLabel12.ValueScaleFactor = 1R
        Me.BasicLabel12.ValueSuffix = Nothing
        '
        'TempController2
        '
        Me.TempController2.Button1Text = Nothing
        Me.TempController2.Button2Text = Nothing
        Me.TempController2.ComComponent = Me.ModbusTCPCom1
        Me.TempController2.DecimalPosition = 0
        Me.TempController2.ForeColor = System.Drawing.Color.LightGray
        Me.TempController2.Location = New System.Drawing.Point(104, 23)
        Me.TempController2.Name = "TempController2"
        Me.TempController2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.TempController2.PLCAddressClick1 = "40024"
        Me.TempController2.PLCAddressClick2 = "40025"
        Me.TempController2.PLCAddressClick3 = "40025"
        Me.TempController2.PLCAddressClick4 = "40025"
        Me.TempController2.PLCAddressText = ""
        Me.TempController2.PLCAddressValuePV = "40024"
        Me.TempController2.PLCAddressValueSP = "40025"
        Me.TempController2.PLCAddressVisible = ""
        Me.TempController2.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController2.Size = New System.Drawing.Size(137, 138)
        Me.TempController2.TabIndex = 7
        Me.TempController2.Value_ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController2.Value_ValueScaleFactorSP = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController2.Value2Text = "SP"
        Me.TempController2.ValuePV = 0!
        Me.TempController2.ValueSP = 0!
        '
        'GroupPanel11
        '
        Me.GroupPanel11.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel11.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel11.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel11.Controls.Add(Me.BasicButton5)
        Me.GroupPanel11.Controls.Add(Me.BasicButton6)
        Me.GroupPanel11.Controls.Add(Me.BasicLabel8)
        Me.GroupPanel11.Controls.Add(Me.TempController1)
        Me.GroupPanel11.Location = New System.Drawing.Point(18, 48)
        Me.GroupPanel11.Name = "GroupPanel11"
        Me.GroupPanel11.SelectBackColor2 = false
        Me.GroupPanel11.Size = New System.Drawing.Size(257, 172)
        Me.GroupPanel11.TabIndex = 15
        '
        'BasicButton5
        '
        Me.BasicButton5.BackColor = System.Drawing.Color.Black
        Me.BasicButton5.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton5.Highlight = true
        Me.BasicButton5.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton5.Location = New System.Drawing.Point(8, 109)
        Me.BasicButton5.MaximumHoldTime = 3000
        Me.BasicButton5.MinimumHoldTime = 500
        Me.BasicButton5.Name = "BasicButton5"
        Me.BasicButton5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton5.PLCAddressClick = "40023"
        Me.BasicButton5.SelectTextAlternate = false
        Me.BasicButton5.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton5.TabIndex = 17
        Me.BasicButton5.Text = "On"
        Me.BasicButton5.TextAlternate = Nothing
        Me.BasicButton5.UseVisualStyleBackColor = false
        Me.BasicButton5.ValueToWrite = 70
        '
        'BasicButton6
        '
        Me.BasicButton6.BackColor = System.Drawing.Color.Black
        Me.BasicButton6.ComComponent = Me.ModbusTCPCom1
        Me.BasicButton6.Highlight = true
        Me.BasicButton6.HighlightColor = System.Drawing.Color.Firebrick
        Me.BasicButton6.Location = New System.Drawing.Point(8, 138)
        Me.BasicButton6.MaximumHoldTime = 3000
        Me.BasicButton6.MinimumHoldTime = 500
        Me.BasicButton6.Name = "BasicButton6"
        Me.BasicButton6.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.BasicButton6.PLCAddressClick = "40023"
        Me.BasicButton6.SelectTextAlternate = false
        Me.BasicButton6.Size = New System.Drawing.Size(75, 23)
        Me.BasicButton6.TabIndex = 16
        Me.BasicButton6.Text = "Off"
        Me.BasicButton6.TextAlternate = Nothing
        Me.BasicButton6.UseVisualStyleBackColor = false
        Me.BasicButton6.ValueToWrite = 0
        '
        'BasicLabel8
        '
        Me.BasicLabel8.AutoSize = true
        Me.BasicLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel8.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel8.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel8.DisplayAsTime = false
        Me.BasicLabel8.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel8.Highlight = false
        Me.BasicLabel8.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel8.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel8.HighlightKeyCharacter = "!"
        Me.BasicLabel8.KeypadAlpahNumeric = false
        Me.BasicLabel8.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel8.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel8.KeypadMaxValue = 0R
        Me.BasicLabel8.KeypadMinValue = 0R
        Me.BasicLabel8.KeypadScaleFactor = 1R
        Me.BasicLabel8.KeypadShowCurrentValue = false
        Me.BasicLabel8.KeypadText = Nothing
        Me.BasicLabel8.KeypadWidth = 300
        Me.BasicLabel8.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel8.Name = "BasicLabel8"
        Me.BasicLabel8.NumericFormat = Nothing
        Me.BasicLabel8.PLCAddressKeypad = ""
        Me.BasicLabel8.PollRate = 0
        Me.BasicLabel8.Size = New System.Drawing.Size(80, 13)
        Me.BasicLabel8.TabIndex = 0
        Me.BasicLabel8.Text = "Ground Floor"
        Me.BasicLabel8.Value = "Ground Floor"
        Me.BasicLabel8.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel8.ValueLeftPadLength = 0
        Me.BasicLabel8.ValuePrefix = Nothing
        Me.BasicLabel8.ValueScaleFactor = 1R
        Me.BasicLabel8.ValueSuffix = Nothing
        '
        'TempController1
        '
        Me.TempController1.Button1Text = Nothing
        Me.TempController1.Button2Text = Nothing
        Me.TempController1.ComComponent = Me.ModbusTCPCom1
        Me.TempController1.DecimalPosition = 0
        Me.TempController1.ForeColor = System.Drawing.Color.LightGray
        Me.TempController1.Location = New System.Drawing.Point(104, 23)
        Me.TempController1.Name = "TempController1"
        Me.TempController1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.TempController1.PLCAddressClick1 = "40022"
        Me.TempController1.PLCAddressClick2 = "40023"
        Me.TempController1.PLCAddressClick3 = "40023"
        Me.TempController1.PLCAddressClick4 = "40023"
        Me.TempController1.PLCAddressText = ""
        Me.TempController1.PLCAddressValuePV = "40022"
        Me.TempController1.PLCAddressValueSP = "40023"
        Me.TempController1.PLCAddressVisible = ""
        Me.TempController1.ScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController1.Size = New System.Drawing.Size(137, 138)
        Me.TempController1.TabIndex = 7
        Me.TempController1.Value_ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController1.Value_ValueScaleFactorSP = New Decimal(New Integer() {1, 0, 0, 0})
        Me.TempController1.Value2Text = "SP"
        Me.TempController1.ValuePV = 0!
        Me.TempController1.ValueSP = 0!
        '
        'BasicLabel11
        '
        Me.BasicLabel11.AutoSize = true
        Me.BasicLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel11.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel11.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel11.DisplayAsTime = false
        Me.BasicLabel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel11.ForeColor = System.Drawing.Color.White
        Me.BasicLabel11.Highlight = false
        Me.BasicLabel11.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel11.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel11.HighlightKeyCharacter = "!"
        Me.BasicLabel11.KeypadAlpahNumeric = false
        Me.BasicLabel11.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel11.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel11.KeypadMaxValue = 0R
        Me.BasicLabel11.KeypadMinValue = 0R
        Me.BasicLabel11.KeypadScaleFactor = 1R
        Me.BasicLabel11.KeypadShowCurrentValue = false
        Me.BasicLabel11.KeypadText = Nothing
        Me.BasicLabel11.KeypadWidth = 300
        Me.BasicLabel11.Location = New System.Drawing.Point(89, 15)
        Me.BasicLabel11.Name = "BasicLabel11"
        Me.BasicLabel11.NumericFormat = Nothing
        Me.BasicLabel11.PLCAddressKeypad = ""
        Me.BasicLabel11.PollRate = 0
        Me.BasicLabel11.Size = New System.Drawing.Size(121, 20)
        Me.BasicLabel11.TabIndex = 2
        Me.BasicLabel11.Text = "HVAC Control"
        Me.BasicLabel11.Value = "HVAC Control"
        Me.BasicLabel11.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel11.ValueLeftPadLength = 0
        Me.BasicLabel11.ValuePrefix = Nothing
        Me.BasicLabel11.ValueScaleFactor = 1R
        Me.BasicLabel11.ValueSuffix = Nothing
        '
        'GroupPanel3
        '
        Me.GroupPanel3.BackColor = System.Drawing.Color.SteelBlue
        Me.GroupPanel3.BackColor2 = System.Drawing.Color.SteelBlue
        Me.GroupPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupPanel3.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel3.Controls.Add(Me.MultiSetButton4)
        Me.GroupPanel3.Controls.Add(Me.MultiSetButton3)
        Me.GroupPanel3.Controls.Add(Me.MultiSetButton2)
        Me.GroupPanel3.Controls.Add(Me.MultiSetButton1)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel8)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel5)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel9)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel7)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel6)
        Me.GroupPanel3.Controls.Add(Me.GroupPanel4)
        Me.GroupPanel3.Controls.Add(Me.BasicLabel1)
        Me.GroupPanel3.Location = New System.Drawing.Point(60, 473)
        Me.GroupPanel3.Name = "GroupPanel3"
        Me.GroupPanel3.SelectBackColor2 = false
        Me.GroupPanel3.Size = New System.Drawing.Size(575, 234)
        Me.GroupPanel3.TabIndex = 42
        '
        'MultiSetButton4
        '
        Me.MultiSetButton4.BackColor = System.Drawing.Color.Black
        Me.MultiSetButton4.ComComponent = Me.ModbusTCPCom1
        Me.MultiSetButton4.Highlight = true
        Me.MultiSetButton4.HighlightColor = System.Drawing.Color.Green
        Me.MultiSetButton4.Location = New System.Drawing.Point(482, 137)
        Me.MultiSetButton4.MaximumHoldTime = 3000
        Me.MultiSetButton4.MinimumHoldTime = 500
        Me.MultiSetButton4.Name = "MultiSetButton4"
        Me.MultiSetButton4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.MultiSetButton4.PLCAddressClick = ""
        Me.MultiSetButton4.PlcValueBegin = 40016
        Me.MultiSetButton4.PlcValueEnd = 40021
        Me.MultiSetButton4.SelectTextAlternate = false
        Me.MultiSetButton4.Size = New System.Drawing.Size(75, 23)
        Me.MultiSetButton4.TabIndex = 25
        Me.MultiSetButton4.Text = "Test"
        Me.MultiSetButton4.TextAlternate = Nothing
        Me.MultiSetButton4.UseVisualStyleBackColor = false
        Me.MultiSetButton4.ValueToWrite = 1
        '
        'MultiSetButton3
        '
        Me.MultiSetButton3.BackColor = System.Drawing.Color.Black
        Me.MultiSetButton3.ComComponent = Me.ModbusTCPCom1
        Me.MultiSetButton3.Highlight = true
        Me.MultiSetButton3.HighlightColor = System.Drawing.Color.Green
        Me.MultiSetButton3.Location = New System.Drawing.Point(482, 108)
        Me.MultiSetButton3.MaximumHoldTime = 3000
        Me.MultiSetButton3.MinimumHoldTime = 500
        Me.MultiSetButton3.Name = "MultiSetButton3"
        Me.MultiSetButton3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.MultiSetButton3.PLCAddressClick = ""
        Me.MultiSetButton3.PlcValueBegin = 40016
        Me.MultiSetButton3.PlcValueEnd = 40021
        Me.MultiSetButton3.SelectTextAlternate = false
        Me.MultiSetButton3.Size = New System.Drawing.Size(75, 23)
        Me.MultiSetButton3.TabIndex = 24
        Me.MultiSetButton3.Text = "Reset"
        Me.MultiSetButton3.TextAlternate = Nothing
        Me.MultiSetButton3.UseVisualStyleBackColor = false
        Me.MultiSetButton3.ValueToWrite = 0
        '
        'MultiSetButton2
        '
        Me.MultiSetButton2.BackColor = System.Drawing.Color.Black
        Me.MultiSetButton2.ComComponent = Me.ModbusTCPCom1
        Me.MultiSetButton2.Highlight = true
        Me.MultiSetButton2.HighlightColor = System.Drawing.Color.Green
        Me.MultiSetButton2.Location = New System.Drawing.Point(482, 79)
        Me.MultiSetButton2.MaximumHoldTime = 3000
        Me.MultiSetButton2.MinimumHoldTime = 500
        Me.MultiSetButton2.Name = "MultiSetButton2"
        Me.MultiSetButton2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.MultiSetButton2.PLCAddressClick = ""
        Me.MultiSetButton2.PlcValueBegin = 40016
        Me.MultiSetButton2.PlcValueEnd = 40021
        Me.MultiSetButton2.SelectTextAlternate = false
        Me.MultiSetButton2.Size = New System.Drawing.Size(75, 23)
        Me.MultiSetButton2.TabIndex = 23
        Me.MultiSetButton2.Text = "Off"
        Me.MultiSetButton2.TextAlternate = Nothing
        Me.MultiSetButton2.UseVisualStyleBackColor = false
        Me.MultiSetButton2.ValueToWrite = -1
        '
        'MultiSetButton1
        '
        Me.MultiSetButton1.BackColor = System.Drawing.Color.Black
        Me.MultiSetButton1.ComComponent = Me.ModbusTCPCom1
        Me.MultiSetButton1.Highlight = true
        Me.MultiSetButton1.HighlightColor = System.Drawing.Color.Green
        Me.MultiSetButton1.Location = New System.Drawing.Point(482, 50)
        Me.MultiSetButton1.MaximumHoldTime = 3000
        Me.MultiSetButton1.MinimumHoldTime = 500
        Me.MultiSetButton1.Name = "MultiSetButton1"
        Me.MultiSetButton1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.MultiSetButton1.PLCAddressClick = ""
        Me.MultiSetButton1.PlcValueBegin = 40016
        Me.MultiSetButton1.PlcValueEnd = 40021
        Me.MultiSetButton1.SelectTextAlternate = false
        Me.MultiSetButton1.Size = New System.Drawing.Size(75, 23)
        Me.MultiSetButton1.TabIndex = 22
        Me.MultiSetButton1.Text = "On"
        Me.MultiSetButton1.TextAlternate = Nothing
        Me.MultiSetButton1.UseVisualStyleBackColor = false
        Me.MultiSetButton1.ValueToWrite = 0
        '
        'GroupPanel8
        '
        Me.GroupPanel8.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel8.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel8.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel8.Controls.Add(Me.AlarmState5)
        Me.GroupPanel8.Controls.Add(Me.BasicLabel9)
        Me.GroupPanel8.Location = New System.Drawing.Point(211, 168)
        Me.GroupPanel8.Name = "GroupPanel8"
        Me.GroupPanel8.SelectBackColor2 = false
        Me.GroupPanel8.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel8.TabIndex = 21
        '
        'AlarmState5
        '
        Me.AlarmState5.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState5.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState5.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState5.Name = "AlarmState5"
        Me.AlarmState5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState5.PLCAddressClick = ""
        Me.AlarmState5.PLCAddressText = ""
        Me.AlarmState5.PLCAddressValue = "40021"
        Me.AlarmState5.PLCAddressVisible = ""
        Me.AlarmState5.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState5.TabIndex = 18
        Me.AlarmState5.Text = "Normal"
        Me.AlarmState5.Value = false
        '
        'BasicLabel9
        '
        Me.BasicLabel9.AutoSize = true
        Me.BasicLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel9.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel9.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel9.DisplayAsTime = false
        Me.BasicLabel9.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel9.Highlight = false
        Me.BasicLabel9.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel9.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel9.HighlightKeyCharacter = "!"
        Me.BasicLabel9.KeypadAlpahNumeric = false
        Me.BasicLabel9.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel9.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel9.KeypadMaxValue = 0R
        Me.BasicLabel9.KeypadMinValue = 0R
        Me.BasicLabel9.KeypadScaleFactor = 1R
        Me.BasicLabel9.KeypadShowCurrentValue = false
        Me.BasicLabel9.KeypadText = Nothing
        Me.BasicLabel9.KeypadWidth = 300
        Me.BasicLabel9.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel9.Name = "BasicLabel9"
        Me.BasicLabel9.NumericFormat = Nothing
        Me.BasicLabel9.PLCAddressKeypad = ""
        Me.BasicLabel9.PollRate = 0
        Me.BasicLabel9.Size = New System.Drawing.Size(79, 13)
        Me.BasicLabel9.TabIndex = 0
        Me.BasicLabel9.Text = "Biology Labs"
        Me.BasicLabel9.Value = "Biology Labs"
        Me.BasicLabel9.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel9.ValueLeftPadLength = 0
        Me.BasicLabel9.ValuePrefix = Nothing
        Me.BasicLabel9.ValueScaleFactor = 1R
        Me.BasicLabel9.ValueSuffix = Nothing
        '
        'GroupPanel5
        '
        Me.GroupPanel5.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel5.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel5.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel5.Controls.Add(Me.AlarmState3)
        Me.GroupPanel5.Controls.Add(Me.BasicLabel5)
        Me.GroupPanel5.Location = New System.Drawing.Point(211, 108)
        Me.GroupPanel5.Name = "GroupPanel5"
        Me.GroupPanel5.SelectBackColor2 = false
        Me.GroupPanel5.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel5.TabIndex = 19
        '
        'AlarmState3
        '
        Me.AlarmState3.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState3.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState3.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState3.Name = "AlarmState3"
        Me.AlarmState3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState3.PLCAddressClick = ""
        Me.AlarmState3.PLCAddressText = ""
        Me.AlarmState3.PLCAddressValue = "40019"
        Me.AlarmState3.PLCAddressVisible = ""
        Me.AlarmState3.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState3.TabIndex = 18
        Me.AlarmState3.Text = "Normal"
        Me.AlarmState3.Value = false
        '
        'BasicLabel5
        '
        Me.BasicLabel5.AutoSize = true
        Me.BasicLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel5.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel5.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel5.DisplayAsTime = false
        Me.BasicLabel5.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel5.Highlight = false
        Me.BasicLabel5.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel5.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel5.HighlightKeyCharacter = "!"
        Me.BasicLabel5.KeypadAlpahNumeric = false
        Me.BasicLabel5.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel5.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel5.KeypadMaxValue = 0R
        Me.BasicLabel5.KeypadMinValue = 0R
        Me.BasicLabel5.KeypadScaleFactor = 1R
        Me.BasicLabel5.KeypadShowCurrentValue = false
        Me.BasicLabel5.KeypadText = Nothing
        Me.BasicLabel5.KeypadWidth = 300
        Me.BasicLabel5.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel5.Name = "BasicLabel5"
        Me.BasicLabel5.NumericFormat = Nothing
        Me.BasicLabel5.PLCAddressKeypad = ""
        Me.BasicLabel5.PollRate = 0
        Me.BasicLabel5.Size = New System.Drawing.Size(34, 13)
        Me.BasicLabel5.TabIndex = 0
        Me.BasicLabel5.Text = "Roof"
        Me.BasicLabel5.Value = "Roof"
        Me.BasicLabel5.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel5.ValueLeftPadLength = 0
        Me.BasicLabel5.ValuePrefix = Nothing
        Me.BasicLabel5.ValueScaleFactor = 1R
        Me.BasicLabel5.ValueSuffix = Nothing
        '
        'GroupPanel9
        '
        Me.GroupPanel9.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel9.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel9.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel9.Controls.Add(Me.AlarmState6)
        Me.GroupPanel9.Controls.Add(Me.BasicLabel10)
        Me.GroupPanel9.Location = New System.Drawing.Point(18, 168)
        Me.GroupPanel9.Name = "GroupPanel9"
        Me.GroupPanel9.SelectBackColor2 = false
        Me.GroupPanel9.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel9.TabIndex = 20
        '
        'AlarmState6
        '
        Me.AlarmState6.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState6.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState6.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState6.Name = "AlarmState6"
        Me.AlarmState6.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState6.PLCAddressClick = ""
        Me.AlarmState6.PLCAddressText = ""
        Me.AlarmState6.PLCAddressValue = "40020"
        Me.AlarmState6.PLCAddressVisible = ""
        Me.AlarmState6.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState6.TabIndex = 17
        Me.AlarmState6.Text = "Normal"
        Me.AlarmState6.Value = false
        '
        'BasicLabel10
        '
        Me.BasicLabel10.AutoSize = true
        Me.BasicLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel10.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel10.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel10.DisplayAsTime = false
        Me.BasicLabel10.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel10.Highlight = false
        Me.BasicLabel10.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel10.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel10.HighlightKeyCharacter = "!"
        Me.BasicLabel10.KeypadAlpahNumeric = false
        Me.BasicLabel10.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel10.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel10.KeypadMaxValue = 0R
        Me.BasicLabel10.KeypadMinValue = 0R
        Me.BasicLabel10.KeypadScaleFactor = 1R
        Me.BasicLabel10.KeypadShowCurrentValue = false
        Me.BasicLabel10.KeypadText = Nothing
        Me.BasicLabel10.KeypadWidth = 300
        Me.BasicLabel10.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel10.Name = "BasicLabel10"
        Me.BasicLabel10.NumericFormat = Nothing
        Me.BasicLabel10.PLCAddressKeypad = ""
        Me.BasicLabel10.PollRate = 0
        Me.BasicLabel10.Size = New System.Drawing.Size(86, 13)
        Me.BasicLabel10.TabIndex = 0
        Me.BasicLabel10.Text = "Rear Stairwell"
        Me.BasicLabel10.Value = "Rear Stairwell"
        Me.BasicLabel10.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel10.ValueLeftPadLength = 0
        Me.BasicLabel10.ValuePrefix = Nothing
        Me.BasicLabel10.ValueScaleFactor = 1R
        Me.BasicLabel10.ValueSuffix = Nothing
        '
        'GroupPanel7
        '
        Me.GroupPanel7.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel7.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel7.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel7.Controls.Add(Me.AlarmState4)
        Me.GroupPanel7.Controls.Add(Me.BasicLabel7)
        Me.GroupPanel7.Location = New System.Drawing.Point(18, 108)
        Me.GroupPanel7.Name = "GroupPanel7"
        Me.GroupPanel7.SelectBackColor2 = false
        Me.GroupPanel7.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel7.TabIndex = 18
        '
        'AlarmState4
        '
        Me.AlarmState4.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState4.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState4.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState4.Name = "AlarmState4"
        Me.AlarmState4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState4.PLCAddressClick = ""
        Me.AlarmState4.PLCAddressText = ""
        Me.AlarmState4.PLCAddressValue = "40018"
        Me.AlarmState4.PLCAddressVisible = ""
        Me.AlarmState4.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState4.TabIndex = 17
        Me.AlarmState4.Text = "Normal"
        Me.AlarmState4.Value = false
        '
        'BasicLabel7
        '
        Me.BasicLabel7.AutoSize = true
        Me.BasicLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel7.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel7.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel7.DisplayAsTime = false
        Me.BasicLabel7.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel7.Highlight = false
        Me.BasicLabel7.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel7.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel7.HighlightKeyCharacter = "!"
        Me.BasicLabel7.KeypadAlpahNumeric = false
        Me.BasicLabel7.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel7.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel7.KeypadMaxValue = 0R
        Me.BasicLabel7.KeypadMinValue = 0R
        Me.BasicLabel7.KeypadScaleFactor = 1R
        Me.BasicLabel7.KeypadShowCurrentValue = false
        Me.BasicLabel7.KeypadText = Nothing
        Me.BasicLabel7.KeypadWidth = 300
        Me.BasicLabel7.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel7.Name = "BasicLabel7"
        Me.BasicLabel7.NumericFormat = Nothing
        Me.BasicLabel7.PLCAddressKeypad = ""
        Me.BasicLabel7.PollRate = 0
        Me.BasicLabel7.Size = New System.Drawing.Size(68, 13)
        Me.BasicLabel7.TabIndex = 0
        Me.BasicLabel7.Text = "Third Floor"
        Me.BasicLabel7.Value = "Third Floor"
        Me.BasicLabel7.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel7.ValueLeftPadLength = 0
        Me.BasicLabel7.ValuePrefix = Nothing
        Me.BasicLabel7.ValueScaleFactor = 1R
        Me.BasicLabel7.ValueSuffix = Nothing
        '
        'GroupPanel6
        '
        Me.GroupPanel6.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel6.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel6.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel6.Controls.Add(Me.AlarmState2)
        Me.GroupPanel6.Controls.Add(Me.BasicLabel6)
        Me.GroupPanel6.Location = New System.Drawing.Point(211, 50)
        Me.GroupPanel6.Name = "GroupPanel6"
        Me.GroupPanel6.SelectBackColor2 = false
        Me.GroupPanel6.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel6.TabIndex = 14
        '
        'AlarmState2
        '
        Me.AlarmState2.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState2.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState2.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState2.Name = "AlarmState2"
        Me.AlarmState2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState2.PLCAddressClick = ""
        Me.AlarmState2.PLCAddressText = ""
        Me.AlarmState2.PLCAddressValue = "40017"
        Me.AlarmState2.PLCAddressVisible = ""
        Me.AlarmState2.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState2.TabIndex = 18
        Me.AlarmState2.Text = "Normal"
        Me.AlarmState2.Value = false
        '
        'BasicLabel6
        '
        Me.BasicLabel6.AutoSize = true
        Me.BasicLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel6.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel6.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel6.DisplayAsTime = false
        Me.BasicLabel6.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel6.Highlight = false
        Me.BasicLabel6.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel6.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel6.HighlightKeyCharacter = "!"
        Me.BasicLabel6.KeypadAlpahNumeric = false
        Me.BasicLabel6.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel6.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel6.KeypadMaxValue = 0R
        Me.BasicLabel6.KeypadMinValue = 0R
        Me.BasicLabel6.KeypadScaleFactor = 1R
        Me.BasicLabel6.KeypadShowCurrentValue = false
        Me.BasicLabel6.KeypadText = Nothing
        Me.BasicLabel6.KeypadWidth = 300
        Me.BasicLabel6.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel6.Name = "BasicLabel6"
        Me.BasicLabel6.NumericFormat = Nothing
        Me.BasicLabel6.PLCAddressKeypad = ""
        Me.BasicLabel6.PollRate = 0
        Me.BasicLabel6.Size = New System.Drawing.Size(82, 13)
        Me.BasicLabel6.TabIndex = 0
        Me.BasicLabel6.Text = "Second Floor"
        Me.BasicLabel6.Value = "Second Floor"
        Me.BasicLabel6.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel6.ValueLeftPadLength = 0
        Me.BasicLabel6.ValuePrefix = Nothing
        Me.BasicLabel6.ValueScaleFactor = 1R
        Me.BasicLabel6.ValueSuffix = Nothing
        '
        'GroupPanel4
        '
        Me.GroupPanel4.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel4.BackColor2 = System.Drawing.SystemColors.ScrollBar
        Me.GroupPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.GroupPanel4.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel4.Controls.Add(Me.AlarmState1)
        Me.GroupPanel4.Controls.Add(Me.BasicLabel2)
        Me.GroupPanel4.Location = New System.Drawing.Point(18, 50)
        Me.GroupPanel4.Name = "GroupPanel4"
        Me.GroupPanel4.SelectBackColor2 = false
        Me.GroupPanel4.Size = New System.Drawing.Size(191, 54)
        Me.GroupPanel4.TabIndex = 7
        '
        'AlarmState1
        '
        Me.AlarmState1.ComComponent = Me.ModbusTCPCom1
        Me.AlarmState1.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.AlarmState1.Location = New System.Drawing.Point(89, 10)
        Me.AlarmState1.Name = "AlarmState1"
        Me.AlarmState1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.AlarmState1.PLCAddressClick = ""
        Me.AlarmState1.PLCAddressText = ""
        Me.AlarmState1.PLCAddressValue = "40016"
        Me.AlarmState1.PLCAddressVisible = ""
        Me.AlarmState1.Size = New System.Drawing.Size(94, 33)
        Me.AlarmState1.TabIndex = 17
        Me.AlarmState1.Text = "Normal"
        Me.AlarmState1.Value = false
        '
        'BasicLabel2
        '
        Me.BasicLabel2.AutoSize = true
        Me.BasicLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel2.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel2.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel2.DisplayAsTime = false
        Me.BasicLabel2.ForeColor = System.Drawing.Color.Black
        Me.BasicLabel2.Highlight = false
        Me.BasicLabel2.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel2.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel2.HighlightKeyCharacter = "!"
        Me.BasicLabel2.KeypadAlpahNumeric = false
        Me.BasicLabel2.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel2.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel2.KeypadMaxValue = 0R
        Me.BasicLabel2.KeypadMinValue = 0R
        Me.BasicLabel2.KeypadScaleFactor = 1R
        Me.BasicLabel2.KeypadShowCurrentValue = false
        Me.BasicLabel2.KeypadText = Nothing
        Me.BasicLabel2.KeypadWidth = 300
        Me.BasicLabel2.Location = New System.Drawing.Point(3, 23)
        Me.BasicLabel2.Name = "BasicLabel2"
        Me.BasicLabel2.NumericFormat = Nothing
        Me.BasicLabel2.PLCAddressKeypad = ""
        Me.BasicLabel2.PollRate = 0
        Me.BasicLabel2.Size = New System.Drawing.Size(80, 13)
        Me.BasicLabel2.TabIndex = 0
        Me.BasicLabel2.Text = "Ground Floor"
        Me.BasicLabel2.Value = "Ground Floor"
        Me.BasicLabel2.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel2.ValueLeftPadLength = 0
        Me.BasicLabel2.ValuePrefix = Nothing
        Me.BasicLabel2.ValueScaleFactor = 1R
        Me.BasicLabel2.ValueSuffix = Nothing
        '
        'BasicLabel1
        '
        Me.BasicLabel1.AutoSize = true
        Me.BasicLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel1.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel1.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel1.DisplayAsTime = false
        Me.BasicLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel1.ForeColor = System.Drawing.Color.White
        Me.BasicLabel1.Highlight = false
        Me.BasicLabel1.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel1.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel1.HighlightKeyCharacter = "!"
        Me.BasicLabel1.KeypadAlpahNumeric = false
        Me.BasicLabel1.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel1.KeypadMaxValue = 0R
        Me.BasicLabel1.KeypadMinValue = 0R
        Me.BasicLabel1.KeypadScaleFactor = 1R
        Me.BasicLabel1.KeypadShowCurrentValue = false
        Me.BasicLabel1.KeypadText = Nothing
        Me.BasicLabel1.KeypadWidth = 300
        Me.BasicLabel1.Location = New System.Drawing.Point(239, 11)
        Me.BasicLabel1.Name = "BasicLabel1"
        Me.BasicLabel1.NumericFormat = Nothing
        Me.BasicLabel1.PLCAddressKeypad = ""
        Me.BasicLabel1.PollRate = 0
        Me.BasicLabel1.Size = New System.Drawing.Size(113, 20)
        Me.BasicLabel1.TabIndex = 2
        Me.BasicLabel1.Text = "Alarm Status"
        Me.BasicLabel1.Value = "Alarm Status"
        Me.BasicLabel1.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel1.ValueLeftPadLength = 0
        Me.BasicLabel1.ValuePrefix = Nothing
        Me.BasicLabel1.ValueScaleFactor = 1R
        Me.BasicLabel1.ValueSuffix = Nothing
        '
        'GroupPanel2
        '
        Me.GroupPanel2.BackColor = System.Drawing.Color.SteelBlue
        Me.GroupPanel2.BackColor2 = System.Drawing.Color.SteelBlue
        Me.GroupPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupPanel2.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel2.Controls.Add(Me.MultiSetButton5)
        Me.GroupPanel2.Controls.Add(Me.MomentaryButton15)
        Me.GroupPanel2.Controls.Add(Me.MomentaryButton14)
        Me.GroupPanel2.Controls.Add(Me.MomentaryButton13)
        Me.GroupPanel2.Controls.Add(Me.Meter23)
        Me.GroupPanel2.Controls.Add(Me.Meter22)
        Me.GroupPanel2.Controls.Add(Me.Meter21)
        Me.GroupPanel2.Controls.Add(Me.BasicLabel4)
        Me.GroupPanel2.Location = New System.Drawing.Point(363, 97)
        Me.GroupPanel2.Name = "GroupPanel2"
        Me.GroupPanel2.SelectBackColor2 = false
        Me.GroupPanel2.Size = New System.Drawing.Size(272, 356)
        Me.GroupPanel2.TabIndex = 39
        '
        'MultiSetButton5
        '
        Me.MultiSetButton5.BackColor = System.Drawing.Color.Black
        Me.MultiSetButton5.ComComponent = Me.ModbusTCPCom1
        Me.MultiSetButton5.Highlight = true
        Me.MultiSetButton5.HighlightColor = System.Drawing.Color.Green
        Me.MultiSetButton5.Location = New System.Drawing.Point(15, 319)
        Me.MultiSetButton5.MaximumHoldTime = 3000
        Me.MultiSetButton5.MinimumHoldTime = 500
        Me.MultiSetButton5.Name = "MultiSetButton5"
        Me.MultiSetButton5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.MultiSetButton5.PLCAddressClick = ""
        Me.MultiSetButton5.PlcValueBegin = 40013
        Me.MultiSetButton5.PlcValueEnd = 40015
        Me.MultiSetButton5.SelectTextAlternate = false
        Me.MultiSetButton5.Size = New System.Drawing.Size(75, 23)
        Me.MultiSetButton5.TabIndex = 48
        Me.MultiSetButton5.Text = "Reset All"
        Me.MultiSetButton5.TextAlternate = Nothing
        Me.MultiSetButton5.UseVisualStyleBackColor = false
        Me.MultiSetButton5.ValueToWrite = 80
        '
        'MomentaryButton15
        '
        Me.MomentaryButton15.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MomentaryButton15.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.RedMushroom
        Me.MomentaryButton15.ComComponent = Me.ModbusTCPCom1
        Me.MomentaryButton15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.MomentaryButton15.ForeColor = System.Drawing.Color.Black
        Me.MomentaryButton15.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large
        Me.MomentaryButton15.Location = New System.Drawing.Point(201, 235)
        Me.MomentaryButton15.MaximumHoldTime = 3000
        Me.MomentaryButton15.MinimumHoldTime = 500
        Me.MomentaryButton15.Name = "MomentaryButton15"
        Me.MomentaryButton15.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.SetFalse
        Me.MomentaryButton15.PLCAddressClick = "40015"
        Me.MomentaryButton15.PLCAddressVisible = ""
        Me.MomentaryButton15.Size = New System.Drawing.Size(53, 78)
        Me.MomentaryButton15.TabIndex = 47
        Me.MomentaryButton15.Text = "Stop"
        '
        'MomentaryButton14
        '
        Me.MomentaryButton14.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MomentaryButton14.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.RedMushroom
        Me.MomentaryButton14.ComComponent = Me.ModbusTCPCom1
        Me.MomentaryButton14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.MomentaryButton14.ForeColor = System.Drawing.Color.Black
        Me.MomentaryButton14.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large
        Me.MomentaryButton14.Location = New System.Drawing.Point(201, 142)
        Me.MomentaryButton14.MaximumHoldTime = 3000
        Me.MomentaryButton14.MinimumHoldTime = 500
        Me.MomentaryButton14.Name = "MomentaryButton14"
        Me.MomentaryButton14.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.SetFalse
        Me.MomentaryButton14.PLCAddressClick = "40014"
        Me.MomentaryButton14.PLCAddressVisible = ""
        Me.MomentaryButton14.Size = New System.Drawing.Size(53, 78)
        Me.MomentaryButton14.TabIndex = 46
        Me.MomentaryButton14.Text = "Stop"
        '
        'MomentaryButton13
        '
        Me.MomentaryButton13.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MomentaryButton13.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.RedMushroom
        Me.MomentaryButton13.ComComponent = Me.ModbusTCPCom1
        Me.MomentaryButton13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.MomentaryButton13.ForeColor = System.Drawing.Color.Black
        Me.MomentaryButton13.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large
        Me.MomentaryButton13.Location = New System.Drawing.Point(201, 48)
        Me.MomentaryButton13.MaximumHoldTime = 3000
        Me.MomentaryButton13.MinimumHoldTime = 500
        Me.MomentaryButton13.Name = "MomentaryButton13"
        Me.MomentaryButton13.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.SetFalse
        Me.MomentaryButton13.PLCAddressClick = "40013"
        Me.MomentaryButton13.PLCAddressVisible = ""
        Me.MomentaryButton13.Size = New System.Drawing.Size(53, 78)
        Me.MomentaryButton13.TabIndex = 45
        Me.MomentaryButton13.Text = "Stop"
        '
        'Meter23
        '
        Me.Meter23.ComComponent = Me.ModbusTCPCom1
        Me.Meter23.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Meter23.HighlightColor = System.Drawing.Color.Red
        Me.Meter23.Location = New System.Drawing.Point(14, 236)
        Me.Meter23.Maximum = 100R
        Me.Meter23.Minimum = 0R
        Me.Meter23.Name = "Meter23"
        Me.Meter23.NumericFormat = Nothing
        Me.Meter23.PLCAddressText = ""
        Me.Meter23.PLCAddressValue = "40015"
        Me.Meter23.PLCAddressVisible = ""
        Me.Meter23.Size = New System.Drawing.Size(173, 77)
        Me.Meter23.TabIndex = 40
        Me.Meter23.Text = "Elevator North"
        Me.Meter23.Value = 50R
        Me.Meter23.ValueScaleFactor = 1R
        '
        'Meter22
        '
        Me.Meter22.ComComponent = Me.ModbusTCPCom1
        Me.Meter22.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Meter22.HighlightColor = System.Drawing.Color.Red
        Me.Meter22.Location = New System.Drawing.Point(15, 143)
        Me.Meter22.Maximum = 100R
        Me.Meter22.Minimum = 0R
        Me.Meter22.Name = "Meter22"
        Me.Meter22.NumericFormat = Nothing
        Me.Meter22.PLCAddressText = ""
        Me.Meter22.PLCAddressValue = "40014"
        Me.Meter22.PLCAddressVisible = ""
        Me.Meter22.Size = New System.Drawing.Size(173, 77)
        Me.Meter22.TabIndex = 38
        Me.Meter22.Text = "Elevator West"
        Me.Meter22.Value = 80R
        Me.Meter22.ValueScaleFactor = 1R
        '
        'Meter21
        '
        Me.Meter21.ComComponent = Me.ModbusTCPCom1
        Me.Meter21.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Meter21.HighlightColor = System.Drawing.Color.Red
        Me.Meter21.Location = New System.Drawing.Point(14, 49)
        Me.Meter21.Maximum = 100R
        Me.Meter21.Minimum = 0R
        Me.Meter21.Name = "Meter21"
        Me.Meter21.NumericFormat = Nothing
        Me.Meter21.PLCAddressText = ""
        Me.Meter21.PLCAddressValue = "40013"
        Me.Meter21.PLCAddressVisible = ""
        Me.Meter21.Size = New System.Drawing.Size(173, 77)
        Me.Meter21.TabIndex = 3
        Me.Meter21.Text = "Elevator East"
        Me.Meter21.Value = 20R
        Me.Meter21.ValueScaleFactor = 1R
        '
        'BasicLabel4
        '
        Me.BasicLabel4.AutoSize = true
        Me.BasicLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel4.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel4.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel4.DisplayAsTime = false
        Me.BasicLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel4.ForeColor = System.Drawing.Color.White
        Me.BasicLabel4.Highlight = false
        Me.BasicLabel4.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel4.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel4.HighlightKeyCharacter = "!"
        Me.BasicLabel4.KeypadAlpahNumeric = false
        Me.BasicLabel4.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel4.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel4.KeypadMaxValue = 0R
        Me.BasicLabel4.KeypadMinValue = 0R
        Me.BasicLabel4.KeypadScaleFactor = 1R
        Me.BasicLabel4.KeypadShowCurrentValue = false
        Me.BasicLabel4.KeypadText = Nothing
        Me.BasicLabel4.KeypadWidth = 300
        Me.BasicLabel4.Location = New System.Drawing.Point(67, 15)
        Me.BasicLabel4.Name = "BasicLabel4"
        Me.BasicLabel4.NumericFormat = Nothing
        Me.BasicLabel4.PLCAddressKeypad = ""
        Me.BasicLabel4.PollRate = 0
        Me.BasicLabel4.Size = New System.Drawing.Size(138, 20)
        Me.BasicLabel4.TabIndex = 2
        Me.BasicLabel4.Text = "Elevator Control"
        Me.BasicLabel4.Value = "Elevator Control"
        Me.BasicLabel4.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel4.ValueLeftPadLength = 0
        Me.BasicLabel4.ValuePrefix = Nothing
        Me.BasicLabel4.ValueScaleFactor = 1R
        Me.BasicLabel4.ValueSuffix = Nothing
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.Color.SteelBlue
        Me.GroupPanel1.BackColor2 = System.Drawing.Color.SteelBlue
        Me.GroupPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupPanel1.ComComponent = Me.ModbusTCPCom1
        Me.GroupPanel1.Controls.Add(Me.PilotLight11)
        Me.GroupPanel1.Controls.Add(Me.PilotLight12)
        Me.GroupPanel1.Controls.Add(Me.PilotLight9)
        Me.GroupPanel1.Controls.Add(Me.PilotLight10)
        Me.GroupPanel1.Controls.Add(Me.PilotLight7)
        Me.GroupPanel1.Controls.Add(Me.PilotLight8)
        Me.GroupPanel1.Controls.Add(Me.PilotLight5)
        Me.GroupPanel1.Controls.Add(Me.PilotLight6)
        Me.GroupPanel1.Controls.Add(Me.PilotLight4)
        Me.GroupPanel1.Controls.Add(Me.PilotLight3)
        Me.GroupPanel1.Controls.Add(Me.PilotLight2)
        Me.GroupPanel1.Controls.Add(Me.PilotLight1)
        Me.GroupPanel1.Controls.Add(Me.BasicLabel3)
        Me.GroupPanel1.Location = New System.Drawing.Point(60, 97)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.SelectBackColor2 = false
        Me.GroupPanel1.Size = New System.Drawing.Size(285, 356)
        Me.GroupPanel1.TabIndex = 38
        '
        'PilotLight11
        '
        Me.PilotLight11.Blink = false
        Me.PilotLight11.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight11.ForeColor = System.Drawing.Color.Black
        Me.PilotLight11.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight11.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight11.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight11.Location = New System.Drawing.Point(211, 237)
        Me.PilotLight11.Name = "PilotLight11"
        Me.PilotLight11.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight11.PLCAddressClick = "40012"
        Me.PilotLight11.PLCAddressText = ""
        Me.PilotLight11.PLCAddressValue = "40012"
        Me.PilotLight11.PLCAddressVisible = ""
        Me.PilotLight11.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight11.TabIndex = 36
        Me.PilotLight11.Text = "Freshman Dorm"
        Me.PilotLight11.Value = false
        '
        'PilotLight12
        '
        Me.PilotLight12.Blink = false
        Me.PilotLight12.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight12.ForeColor = System.Drawing.Color.Black
        Me.PilotLight12.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight12.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight12.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight12.Location = New System.Drawing.Point(144, 237)
        Me.PilotLight12.Name = "PilotLight12"
        Me.PilotLight12.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight12.PLCAddressClick = "40011"
        Me.PilotLight12.PLCAddressText = ""
        Me.PilotLight12.PLCAddressValue = "40011"
        Me.PilotLight12.PLCAddressVisible = ""
        Me.PilotLight12.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight12.TabIndex = 35
        Me.PilotLight12.Text = "Containment Wing"
        Me.PilotLight12.Value = false
        '
        'PilotLight9
        '
        Me.PilotLight9.Blink = false
        Me.PilotLight9.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight9.ForeColor = System.Drawing.Color.Black
        Me.PilotLight9.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight9.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight9.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight9.Location = New System.Drawing.Point(77, 237)
        Me.PilotLight9.Name = "PilotLight9"
        Me.PilotLight9.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight9.PLCAddressClick = "40010"
        Me.PilotLight9.PLCAddressText = ""
        Me.PilotLight9.PLCAddressValue = "40010"
        Me.PilotLight9.PLCAddressVisible = ""
        Me.PilotLight9.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight9.TabIndex = 34
        Me.PilotLight9.Text = "Mutagenesis Lab"
        Me.PilotLight9.Value = false
        '
        'PilotLight10
        '
        Me.PilotLight10.Blink = false
        Me.PilotLight10.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight10.ForeColor = System.Drawing.Color.Black
        Me.PilotLight10.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight10.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight10.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight10.Location = New System.Drawing.Point(10, 237)
        Me.PilotLight10.Name = "PilotLight10"
        Me.PilotLight10.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight10.PLCAddressClick = "40009"
        Me.PilotLight10.PLCAddressText = ""
        Me.PilotLight10.PLCAddressValue = "40009"
        Me.PilotLight10.PLCAddressVisible = ""
        Me.PilotLight10.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight10.TabIndex = 33
        Me.PilotLight10.Text = "Biology Lab"
        Me.PilotLight10.Value = false
        '
        'PilotLight7
        '
        Me.PilotLight7.Blink = false
        Me.PilotLight7.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight7.ForeColor = System.Drawing.Color.Black
        Me.PilotLight7.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight7.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight7.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight7.Location = New System.Drawing.Point(211, 143)
        Me.PilotLight7.Name = "PilotLight7"
        Me.PilotLight7.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight7.PLCAddressClick = "40008"
        Me.PilotLight7.PLCAddressText = ""
        Me.PilotLight7.PLCAddressValue = "40008"
        Me.PilotLight7.PLCAddressVisible = ""
        Me.PilotLight7.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight7.TabIndex = 32
        Me.PilotLight7.Text = "Snack Room"
        Me.PilotLight7.Value = false
        '
        'PilotLight8
        '
        Me.PilotLight8.Blink = false
        Me.PilotLight8.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight8.ForeColor = System.Drawing.Color.Black
        Me.PilotLight8.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight8.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight8.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight8.Location = New System.Drawing.Point(144, 143)
        Me.PilotLight8.Name = "PilotLight8"
        Me.PilotLight8.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight8.PLCAddressClick = "40007"
        Me.PilotLight8.PLCAddressText = ""
        Me.PilotLight8.PLCAddressValue = "40007"
        Me.PilotLight8.PLCAddressVisible = ""
        Me.PilotLight8.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight8.TabIndex = 31
        Me.PilotLight8.Text = "Particle Accelerator"
        Me.PilotLight8.Value = false
        '
        'PilotLight5
        '
        Me.PilotLight5.Blink = false
        Me.PilotLight5.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight5.ForeColor = System.Drawing.Color.Black
        Me.PilotLight5.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight5.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight5.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight5.Location = New System.Drawing.Point(77, 143)
        Me.PilotLight5.Name = "PilotLight5"
        Me.PilotLight5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight5.PLCAddressClick = "40006"
        Me.PilotLight5.PLCAddressText = ""
        Me.PilotLight5.PLCAddressValue = "40006"
        Me.PilotLight5.PLCAddressVisible = ""
        Me.PilotLight5.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight5.TabIndex = 30
        Me.PilotLight5.Text = "Testing Room B"
        Me.PilotLight5.Value = false
        '
        'PilotLight6
        '
        Me.PilotLight6.Blink = false
        Me.PilotLight6.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight6.ForeColor = System.Drawing.Color.Black
        Me.PilotLight6.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight6.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight6.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight6.Location = New System.Drawing.Point(10, 143)
        Me.PilotLight6.Name = "PilotLight6"
        Me.PilotLight6.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight6.PLCAddressClick = "40005"
        Me.PilotLight6.PLCAddressText = ""
        Me.PilotLight6.PLCAddressValue = "40005"
        Me.PilotLight6.PLCAddressVisible = ""
        Me.PilotLight6.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight6.TabIndex = 29
        Me.PilotLight6.Text = "Testing Room A"
        Me.PilotLight6.Value = false
        '
        'PilotLight4
        '
        Me.PilotLight4.Blink = false
        Me.PilotLight4.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight4.ForeColor = System.Drawing.Color.Black
        Me.PilotLight4.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight4.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight4.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight4.Location = New System.Drawing.Point(211, 48)
        Me.PilotLight4.Name = "PilotLight4"
        Me.PilotLight4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight4.PLCAddressClick = "40004"
        Me.PilotLight4.PLCAddressText = ""
        Me.PilotLight4.PLCAddressValue = "40004"
        Me.PilotLight4.PLCAddressVisible = ""
        Me.PilotLight4.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight4.TabIndex = 28
        Me.PilotLight4.Text = "Lobby"
        Me.PilotLight4.Value = false
        '
        'PilotLight3
        '
        Me.PilotLight3.Blink = false
        Me.PilotLight3.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight3.ForeColor = System.Drawing.Color.Black
        Me.PilotLight3.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight3.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight3.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight3.Location = New System.Drawing.Point(144, 48)
        Me.PilotLight3.Name = "PilotLight3"
        Me.PilotLight3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight3.PLCAddressClick = "40003"
        Me.PilotLight3.PLCAddressText = ""
        Me.PilotLight3.PLCAddressValue = "40003"
        Me.PilotLight3.PLCAddressVisible = ""
        Me.PilotLight3.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight3.TabIndex = 27
        Me.PilotLight3.Text = "West Entrance"
        Me.PilotLight3.Value = false
        '
        'PilotLight2
        '
        Me.PilotLight2.Blink = false
        Me.PilotLight2.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight2.ForeColor = System.Drawing.Color.Black
        Me.PilotLight2.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight2.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight2.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight2.Location = New System.Drawing.Point(77, 48)
        Me.PilotLight2.Name = "PilotLight2"
        Me.PilotLight2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight2.PLCAddressClick = "40002"
        Me.PilotLight2.PLCAddressText = ""
        Me.PilotLight2.PLCAddressValue = "40002"
        Me.PilotLight2.PLCAddressVisible = ""
        Me.PilotLight2.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight2.TabIndex = 26
        Me.PilotLight2.Text = "West Entrance"
        Me.PilotLight2.Value = false
        '
        'PilotLight1
        '
        Me.PilotLight1.Blink = false
        Me.PilotLight1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PilotLight1.ForeColor = System.Drawing.Color.Black
        Me.PilotLight1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight1.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Green
        Me.PilotLight1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight1.Location = New System.Drawing.Point(10, 48)
        Me.PilotLight1.Name = "PilotLight1"
        Me.PilotLight1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.Toggle
        Me.PilotLight1.PLCAddressClick = "40001"
        Me.PilotLight1.PLCAddressText = ""
        Me.PilotLight1.PLCAddressValue = "40001"
        Me.PilotLight1.PLCAddressVisible = ""
        Me.PilotLight1.Size = New System.Drawing.Size(60, 88)
        Me.PilotLight1.TabIndex = 25
        Me.PilotLight1.Text = "Main Entrance"
        Me.PilotLight1.Value = false
        '
        'BasicLabel3
        '
        Me.BasicLabel3.AutoSize = true
        Me.BasicLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BasicLabel3.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel3.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel3.DisplayAsTime = false
        Me.BasicLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel3.ForeColor = System.Drawing.Color.White
        Me.BasicLabel3.Highlight = false
        Me.BasicLabel3.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel3.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel3.HighlightKeyCharacter = "!"
        Me.BasicLabel3.KeypadAlpahNumeric = false
        Me.BasicLabel3.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel3.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel3.KeypadMaxValue = 0R
        Me.BasicLabel3.KeypadMinValue = 0R
        Me.BasicLabel3.KeypadScaleFactor = 1R
        Me.BasicLabel3.KeypadShowCurrentValue = false
        Me.BasicLabel3.KeypadText = Nothing
        Me.BasicLabel3.KeypadWidth = 300
        Me.BasicLabel3.Location = New System.Drawing.Point(74, 15)
        Me.BasicLabel3.Name = "BasicLabel3"
        Me.BasicLabel3.NumericFormat = Nothing
        Me.BasicLabel3.PLCAddressKeypad = ""
        Me.BasicLabel3.PollRate = 0
        Me.BasicLabel3.Size = New System.Drawing.Size(131, 20)
        Me.BasicLabel3.TabIndex = 2
        Me.BasicLabel3.Text = "Keypad Control"
        Me.BasicLabel3.Value = "Keypad Control"
        Me.BasicLabel3.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel3.ValueLeftPadLength = 0
        Me.BasicLabel3.ValuePrefix = Nothing
        Me.BasicLabel3.ValueScaleFactor = 1R
        Me.BasicLabel3.ValueSuffix = Nothing
        '
        'MainForm
        '
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.GroupPanel14)
        Me.Controls.Add(Me.GroupPanel10)
        Me.Controls.Add(Me.GroupPanel3)
        Me.Controls.Add(Me.GroupPanel2)
        Me.Controls.Add(Me.GroupPanel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.KeyPreview = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Pleasant Valley University"
        Me.GroupPanel14.ResumeLayout(false)
        Me.GroupPanel14.PerformLayout
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupPanel10.ResumeLayout(false)
        Me.GroupPanel10.PerformLayout
        Me.GroupPanel13.ResumeLayout(false)
        Me.GroupPanel13.PerformLayout
        Me.GroupPanel12.ResumeLayout(false)
        Me.GroupPanel12.PerformLayout
        Me.GroupPanel11.ResumeLayout(false)
        Me.GroupPanel11.PerformLayout
        Me.GroupPanel3.ResumeLayout(false)
        Me.GroupPanel3.PerformLayout
        Me.GroupPanel8.ResumeLayout(false)
        Me.GroupPanel8.PerformLayout
        Me.GroupPanel5.ResumeLayout(false)
        Me.GroupPanel5.PerformLayout
        Me.GroupPanel9.ResumeLayout(false)
        Me.GroupPanel9.PerformLayout
        Me.GroupPanel7.ResumeLayout(false)
        Me.GroupPanel7.PerformLayout
        Me.GroupPanel6.ResumeLayout(false)
        Me.GroupPanel6.PerformLayout
        Me.GroupPanel4.ResumeLayout(false)
        Me.GroupPanel4.PerformLayout
        Me.GroupPanel2.ResumeLayout(false)
        Me.GroupPanel2.PerformLayout
        Me.GroupPanel1.ResumeLayout(false)
        Me.GroupPanel1.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents DF1ComWF1 As AdvancedHMIDrivers.SerialDF1forSLCMicroCom
    Friend WithEvents ForceItemsIntoToolBox1 As MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox
    Friend WithEvents ModbusTCPCom1 As AdvancedHMIDrivers.ModbusTCPCom
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents BasicLabel3 As AdvancedHMIControls.BasicLabel
    Friend WithEvents TempController1 As AdvancedHMIControls.TempController
    Friend WithEvents GroupPanel1 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicLabel4 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel2 As AdvancedHMIControls.GroupPanel
    Friend WithEvents Meter23 As AdvancedHMIControls.Meter2
    Friend WithEvents Meter22 As AdvancedHMIControls.Meter2
    Friend WithEvents Meter21 As AdvancedHMIControls.Meter2
    Friend WithEvents GroupPanel3 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicLabel1 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel6 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicLabel6 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel4 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicLabel2 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel10 As AdvancedHMIControls.GroupPanel
    Friend WithEvents GroupPanel13 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicButton9 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicButton10 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicLabel13 As AdvancedHMIControls.BasicLabel
    Friend WithEvents TempController3 As AdvancedHMIControls.TempController
    Friend WithEvents GroupPanel12 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicButton7 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicButton8 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicLabel12 As AdvancedHMIControls.BasicLabel
    Friend WithEvents TempController2 As AdvancedHMIControls.TempController
    Friend WithEvents GroupPanel11 As AdvancedHMIControls.GroupPanel
    Friend WithEvents BasicButton5 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicButton6 As AdvancedHMIControls.BasicButton
    Friend WithEvents BasicLabel8 As AdvancedHMIControls.BasicLabel
    Friend WithEvents BasicLabel11 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel14 As AdvancedHMIControls.GroupPanel
    Friend WithEvents DateTimeDisplay1 As AdvancedHMIControls.DateTimeDisplay
    Friend WithEvents BasicLabel14 As AdvancedHMIControls.BasicLabel
    Friend WithEvents MomentaryButton15 As AdvancedHMIControls.MomentaryButton
    Friend WithEvents MomentaryButton14 As AdvancedHMIControls.MomentaryButton
    Friend WithEvents MomentaryButton13 As AdvancedHMIControls.MomentaryButton
    Friend WithEvents PilotLight3 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight2 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight1 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight11 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight12 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight9 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight10 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight7 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight8 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight5 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight6 As AdvancedHMIControls.PilotLight
    Friend WithEvents PilotLight4 As AdvancedHMIControls.PilotLight
    Friend WithEvents AlarmState1 As AdvancedHMIControls.AlarmState
    Friend WithEvents GroupPanel8 As AdvancedHMIControls.GroupPanel
    Friend WithEvents AlarmState5 As AdvancedHMIControls.AlarmState
    Friend WithEvents BasicLabel9 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel5 As AdvancedHMIControls.GroupPanel
    Friend WithEvents AlarmState3 As AdvancedHMIControls.AlarmState
    Friend WithEvents BasicLabel5 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel9 As AdvancedHMIControls.GroupPanel
    Friend WithEvents AlarmState6 As AdvancedHMIControls.AlarmState
    Friend WithEvents BasicLabel10 As AdvancedHMIControls.BasicLabel
    Friend WithEvents GroupPanel7 As AdvancedHMIControls.GroupPanel
    Friend WithEvents AlarmState4 As AdvancedHMIControls.AlarmState
    Friend WithEvents BasicLabel7 As AdvancedHMIControls.BasicLabel
    Friend WithEvents AlarmState2 As AdvancedHMIControls.AlarmState
    Friend WithEvents MultiSetButton4 As AdvancedHMIControls.MultiSetButton
    Friend WithEvents MultiSetButton3 As AdvancedHMIControls.MultiSetButton
    Friend WithEvents MultiSetButton2 As AdvancedHMIControls.MultiSetButton
    Friend WithEvents MultiSetButton1 As AdvancedHMIControls.MultiSetButton
    Friend WithEvents MultiSetButton5 As AdvancedHMIControls.MultiSetButton
End Class
