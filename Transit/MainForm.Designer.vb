﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '   <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    ' <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ForceItemsIntoToolBox1 = New MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox()
        Me.ModbusTCPCom1 = New AdvancedHMIDrivers.ModbusTCPCom(Me.components)
        Me.PilotLight1 = New AdvancedHMIControls.PilotLight()
        Me.PilotLight3Color7 = New AdvancedHMIControls.PilotLight3Color()
        Me.BasicLabel1 = New AdvancedHMIControls.BasicLabel()
        Me.BasicLabel2 = New AdvancedHMIControls.BasicLabel()
        Me.BasicLabel3 = New AdvancedHMIControls.BasicLabel()
        Me.BasicLabel4 = New AdvancedHMIControls.BasicLabel()
        Me.SelectorSwitch1 = New AdvancedHMIControls.SelectorSwitch()
        Me.SelectorSwitch2 = New AdvancedHMIControls.SelectorSwitch()
        Me.SelectorSwitch3 = New AdvancedHMIControls.SelectorSwitch()
        Me.PilotLight3Color1 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color2 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color3 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color4 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color5 = New AdvancedHMIControls.PilotLight3Color()
        Me.Odometer1 = New AdvancedHMIControls.Odometer()
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'ModbusTCPCom1
        '
        Me.ModbusTCPCom1.DisableSubscriptions = false
        Me.ModbusTCPCom1.IniFileName = ""
        Me.ModbusTCPCom1.IniFileSection = Nothing
        Me.ModbusTCPCom1.IPAddress = "127.0.0.1"
        Me.ModbusTCPCom1.MaxReadGroupSize = 20
        Me.ModbusTCPCom1.SwapBytes = true
        Me.ModbusTCPCom1.SwapWords = false
        Me.ModbusTCPCom1.TcpipPort = CType(502US,UShort)
        Me.ModbusTCPCom1.TimeOut = 3000
        Me.ModbusTCPCom1.UnitId = CType(1,Byte)
        '
        'PilotLight1
        '
        Me.PilotLight1.Blink = false
        Me.PilotLight1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight1.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight1.Location = New System.Drawing.Point(679, 430)
        Me.PilotLight1.Name = "PilotLight1"
        Me.PilotLight1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight1.PLCAddressClick = ""
        Me.PilotLight1.PLCAddressText = ""
        Me.PilotLight1.PLCAddressValue = ""
        Me.PilotLight1.PLCAddressVisible = ""
        Me.PilotLight1.Size = New System.Drawing.Size(75, 110)
        Me.PilotLight1.TabIndex = 73
        Me.PilotLight1.Text = "Emergency Stop"
        Me.PilotLight1.Value = false
        '
        'PilotLight3Color7
        '
        Me.PilotLight3Color7.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color7.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color7.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color7.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color7.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color7.Location = New System.Drawing.Point(366, 130)
        Me.PilotLight3Color7.Name = "PilotLight3Color7"
        Me.PilotLight3Color7.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color7.PLCaddressClick = ""
        Me.PilotLight3Color7.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color7.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color7.PLCaddressText = ""
        Me.PilotLight3Color7.PLCaddressVisible = ""
        Me.PilotLight3Color7.SelectColor2 = true
        Me.PilotLight3Color7.SelectColor3 = true
        Me.PilotLight3Color7.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color7.TabIndex = 67
        '
        'BasicLabel1
        '
        Me.BasicLabel1.AutoSize = true
        Me.BasicLabel1.BackColor = System.Drawing.Color.Black
        Me.BasicLabel1.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel1.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel1.DisplayAsTime = false
        Me.BasicLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel1.ForeColor = System.Drawing.Color.White
        Me.BasicLabel1.Highlight = false
        Me.BasicLabel1.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel1.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel1.HighlightKeyCharacter = "!"
        Me.BasicLabel1.KeypadAlpahNumeric = false
        Me.BasicLabel1.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel1.KeypadMaxValue = 0R
        Me.BasicLabel1.KeypadMinValue = 0R
        Me.BasicLabel1.KeypadScaleFactor = 1R
        Me.BasicLabel1.KeypadShowCurrentValue = false
        Me.BasicLabel1.KeypadText = Nothing
        Me.BasicLabel1.KeypadWidth = 300
        Me.BasicLabel1.Location = New System.Drawing.Point(27, 29)
        Me.BasicLabel1.Name = "BasicLabel1"
        Me.BasicLabel1.NumericFormat = Nothing
        Me.BasicLabel1.PLCAddressKeypad = ""
        Me.BasicLabel1.PollRate = 0
        Me.BasicLabel1.Size = New System.Drawing.Size(197, 26)
        Me.BasicLabel1.TabIndex = 74
        Me.BasicLabel1.Text = "Switching Station"
        Me.BasicLabel1.Value = "Switching Station"
        Me.BasicLabel1.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel1.ValueLeftPadLength = 0
        Me.BasicLabel1.ValuePrefix = Nothing
        Me.BasicLabel1.ValueScaleFactor = 1R
        Me.BasicLabel1.ValueSuffix = Nothing
        '
        'BasicLabel2
        '
        Me.BasicLabel2.AutoSize = true
        Me.BasicLabel2.BackColor = System.Drawing.Color.Black
        Me.BasicLabel2.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel2.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel2.DisplayAsTime = false
        Me.BasicLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel2.ForeColor = System.Drawing.Color.White
        Me.BasicLabel2.Highlight = false
        Me.BasicLabel2.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel2.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel2.HighlightKeyCharacter = "!"
        Me.BasicLabel2.KeypadAlpahNumeric = false
        Me.BasicLabel2.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel2.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel2.KeypadMaxValue = 0R
        Me.BasicLabel2.KeypadMinValue = 0R
        Me.BasicLabel2.KeypadScaleFactor = 1R
        Me.BasicLabel2.KeypadShowCurrentValue = false
        Me.BasicLabel2.KeypadText = Nothing
        Me.BasicLabel2.KeypadWidth = 300
        Me.BasicLabel2.Location = New System.Drawing.Point(28, 145)
        Me.BasicLabel2.Name = "BasicLabel2"
        Me.BasicLabel2.NumericFormat = Nothing
        Me.BasicLabel2.PLCAddressKeypad = ""
        Me.BasicLabel2.PollRate = 0
        Me.BasicLabel2.Size = New System.Drawing.Size(115, 20)
        Me.BasicLabel2.TabIndex = 75
        Me.BasicLabel2.Text = "Switch South"
        Me.BasicLabel2.Value = "Switch South"
        Me.BasicLabel2.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel2.ValueLeftPadLength = 0
        Me.BasicLabel2.ValuePrefix = Nothing
        Me.BasicLabel2.ValueScaleFactor = 1R
        Me.BasicLabel2.ValueSuffix = Nothing
        '
        'BasicLabel3
        '
        Me.BasicLabel3.AutoSize = true
        Me.BasicLabel3.BackColor = System.Drawing.Color.Black
        Me.BasicLabel3.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel3.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel3.DisplayAsTime = false
        Me.BasicLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel3.ForeColor = System.Drawing.Color.White
        Me.BasicLabel3.Highlight = false
        Me.BasicLabel3.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel3.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel3.HighlightKeyCharacter = "!"
        Me.BasicLabel3.KeypadAlpahNumeric = false
        Me.BasicLabel3.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel3.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel3.KeypadMaxValue = 0R
        Me.BasicLabel3.KeypadMinValue = 0R
        Me.BasicLabel3.KeypadScaleFactor = 1R
        Me.BasicLabel3.KeypadShowCurrentValue = false
        Me.BasicLabel3.KeypadText = Nothing
        Me.BasicLabel3.KeypadWidth = 300
        Me.BasicLabel3.Location = New System.Drawing.Point(28, 274)
        Me.BasicLabel3.Name = "BasicLabel3"
        Me.BasicLabel3.NumericFormat = Nothing
        Me.BasicLabel3.PLCAddressKeypad = ""
        Me.BasicLabel3.PollRate = 0
        Me.BasicLabel3.Size = New System.Drawing.Size(111, 20)
        Me.BasicLabel3.TabIndex = 76
        Me.BasicLabel3.Text = "Switch North"
        Me.BasicLabel3.Value = "Switch North"
        Me.BasicLabel3.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel3.ValueLeftPadLength = 0
        Me.BasicLabel3.ValuePrefix = Nothing
        Me.BasicLabel3.ValueScaleFactor = 1R
        Me.BasicLabel3.ValueSuffix = Nothing
        '
        'BasicLabel4
        '
        Me.BasicLabel4.AutoSize = true
        Me.BasicLabel4.BackColor = System.Drawing.Color.Black
        Me.BasicLabel4.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel4.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel4.DisplayAsTime = false
        Me.BasicLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel4.ForeColor = System.Drawing.Color.White
        Me.BasicLabel4.Highlight = false
        Me.BasicLabel4.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel4.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel4.HighlightKeyCharacter = "!"
        Me.BasicLabel4.KeypadAlpahNumeric = false
        Me.BasicLabel4.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel4.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel4.KeypadMaxValue = 0R
        Me.BasicLabel4.KeypadMinValue = 0R
        Me.BasicLabel4.KeypadScaleFactor = 1R
        Me.BasicLabel4.KeypadShowCurrentValue = false
        Me.BasicLabel4.KeypadText = Nothing
        Me.BasicLabel4.KeypadWidth = 300
        Me.BasicLabel4.Location = New System.Drawing.Point(28, 398)
        Me.BasicLabel4.Name = "BasicLabel4"
        Me.BasicLabel4.NumericFormat = Nothing
        Me.BasicLabel4.PLCAddressKeypad = ""
        Me.BasicLabel4.PollRate = 0
        Me.BasicLabel4.Size = New System.Drawing.Size(148, 20)
        Me.BasicLabel4.TabIndex = 77
        Me.BasicLabel4.Text = "Switch Northwest"
        Me.BasicLabel4.Value = "Switch Northwest"
        Me.BasicLabel4.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel4.ValueLeftPadLength = 0
        Me.BasicLabel4.ValuePrefix = Nothing
        Me.BasicLabel4.ValueScaleFactor = 1R
        Me.BasicLabel4.ValueSuffix = Nothing
        '
        'SelectorSwitch1
        '
        Me.SelectorSwitch1.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch1.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch1.Location = New System.Drawing.Point(250, 113)
        Me.SelectorSwitch1.Name = "SelectorSwitch1"
        Me.SelectorSwitch1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SelectorSwitch1.PLCAddressClick = ""
        Me.SelectorSwitch1.PLCAddressText = ""
        Me.SelectorSwitch1.PLCAddressValue = ""
        Me.SelectorSwitch1.PLCAddressVisible = ""
        Me.SelectorSwitch1.Size = New System.Drawing.Size(61, 89)
        Me.SelectorSwitch1.TabIndex = 78
        Me.SelectorSwitch1.Value = false
        '
        'SelectorSwitch2
        '
        Me.SelectorSwitch2.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch2.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch2.Location = New System.Drawing.Point(250, 236)
        Me.SelectorSwitch2.Name = "SelectorSwitch2"
        Me.SelectorSwitch2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SelectorSwitch2.PLCAddressClick = ""
        Me.SelectorSwitch2.PLCAddressText = ""
        Me.SelectorSwitch2.PLCAddressValue = ""
        Me.SelectorSwitch2.PLCAddressVisible = ""
        Me.SelectorSwitch2.Size = New System.Drawing.Size(61, 89)
        Me.SelectorSwitch2.TabIndex = 79
        Me.SelectorSwitch2.Value = false
        '
        'SelectorSwitch3
        '
        Me.SelectorSwitch3.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch.LegendPlates.Large
        Me.SelectorSwitch3.Location = New System.Drawing.Point(250, 357)
        Me.SelectorSwitch3.Name = "SelectorSwitch3"
        Me.SelectorSwitch3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SelectorSwitch3.PLCAddressClick = ""
        Me.SelectorSwitch3.PLCAddressText = ""
        Me.SelectorSwitch3.PLCAddressValue = ""
        Me.SelectorSwitch3.PLCAddressVisible = ""
        Me.SelectorSwitch3.Size = New System.Drawing.Size(61, 89)
        Me.SelectorSwitch3.TabIndex = 80
        Me.SelectorSwitch3.Value = false
        '
        'PilotLight3Color1
        '
        Me.PilotLight3Color1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color1.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color1.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color1.Location = New System.Drawing.Point(445, 130)
        Me.PilotLight3Color1.Name = "PilotLight3Color1"
        Me.PilotLight3Color1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color1.PLCaddressClick = ""
        Me.PilotLight3Color1.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color1.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color1.PLCaddressText = ""
        Me.PilotLight3Color1.PLCaddressVisible = ""
        Me.PilotLight3Color1.SelectColor2 = true
        Me.PilotLight3Color1.SelectColor3 = true
        Me.PilotLight3Color1.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color1.TabIndex = 81
        '
        'PilotLight3Color2
        '
        Me.PilotLight3Color2.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color2.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color2.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color2.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color2.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color2.Location = New System.Drawing.Point(445, 250)
        Me.PilotLight3Color2.Name = "PilotLight3Color2"
        Me.PilotLight3Color2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color2.PLCaddressClick = ""
        Me.PilotLight3Color2.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color2.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color2.PLCaddressText = ""
        Me.PilotLight3Color2.PLCaddressVisible = ""
        Me.PilotLight3Color2.SelectColor2 = true
        Me.PilotLight3Color2.SelectColor3 = true
        Me.PilotLight3Color2.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color2.TabIndex = 83
        '
        'PilotLight3Color3
        '
        Me.PilotLight3Color3.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color3.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color3.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color3.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color3.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color3.Location = New System.Drawing.Point(366, 250)
        Me.PilotLight3Color3.Name = "PilotLight3Color3"
        Me.PilotLight3Color3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color3.PLCaddressClick = ""
        Me.PilotLight3Color3.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color3.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color3.PLCaddressText = ""
        Me.PilotLight3Color3.PLCaddressVisible = ""
        Me.PilotLight3Color3.SelectColor2 = true
        Me.PilotLight3Color3.SelectColor3 = true
        Me.PilotLight3Color3.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color3.TabIndex = 82
        '
        'PilotLight3Color4
        '
        Me.PilotLight3Color4.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color4.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color4.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color4.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color4.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color4.Location = New System.Drawing.Point(445, 374)
        Me.PilotLight3Color4.Name = "PilotLight3Color4"
        Me.PilotLight3Color4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color4.PLCaddressClick = ""
        Me.PilotLight3Color4.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color4.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color4.PLCaddressText = ""
        Me.PilotLight3Color4.PLCaddressVisible = ""
        Me.PilotLight3Color4.SelectColor2 = true
        Me.PilotLight3Color4.SelectColor3 = true
        Me.PilotLight3Color4.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color4.TabIndex = 85
        '
        'PilotLight3Color5
        '
        Me.PilotLight3Color5.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color5.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color5.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color5.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color5.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color5.Location = New System.Drawing.Point(366, 374)
        Me.PilotLight3Color5.Name = "PilotLight3Color5"
        Me.PilotLight3Color5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color5.PLCaddressClick = ""
        Me.PilotLight3Color5.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color5.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color5.PLCaddressText = ""
        Me.PilotLight3Color5.PLCaddressVisible = ""
        Me.PilotLight3Color5.SelectColor2 = true
        Me.PilotLight3Color5.SelectColor3 = true
        Me.PilotLight3Color5.Size = New System.Drawing.Size(54, 57)
        Me.PilotLight3Color5.TabIndex = 84
        '
        'Odometer1
        '
        Me.Odometer1.BackColorAfterDecimal = System.Drawing.Color.Black
        Me.Odometer1.ComComponent = Me.ModbusTCPCom1
        Me.Odometer1.ForeColorAfterDecimal = System.Drawing.Color.White
        Me.Odometer1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.Odometer1.KeypadMaxValue = 0R
        Me.Odometer1.KeypadMinValue = 0R
        Me.Odometer1.KeypadScaleFactor = 1R
        Me.Odometer1.KeypadText = Nothing
        Me.Odometer1.KeypadWidth = 300
        Me.Odometer1.Location = New System.Drawing.Point(628, 29)
        Me.Odometer1.Name = "Odometer1"
        Me.Odometer1.NumberOfDigits = 5
        Me.Odometer1.NumberOfDigitsAfterDecimal = 1
        Me.Odometer1.PLCAddressKeypad = ""
        Me.Odometer1.PLCAddressValue = Nothing
        Me.Odometer1.Size = New System.Drawing.Size(126, 52)
        Me.Odometer1.TabIndex = 86
        Me.Odometer1.Text = "Odometer1"
        Me.Odometer1.Value = 0R
        '
        'MainForm
        '
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.Odometer1)
        Me.Controls.Add(Me.PilotLight3Color4)
        Me.Controls.Add(Me.PilotLight3Color5)
        Me.Controls.Add(Me.PilotLight3Color2)
        Me.Controls.Add(Me.PilotLight3Color3)
        Me.Controls.Add(Me.PilotLight3Color1)
        Me.Controls.Add(Me.SelectorSwitch3)
        Me.Controls.Add(Me.SelectorSwitch2)
        Me.Controls.Add(Me.SelectorSwitch1)
        Me.Controls.Add(Me.BasicLabel4)
        Me.Controls.Add(Me.BasicLabel3)
        Me.Controls.Add(Me.BasicLabel2)
        Me.Controls.Add(Me.BasicLabel1)
        Me.Controls.Add(Me.PilotLight1)
        Me.Controls.Add(Me.PilotLight3Color7)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.KeyPreview = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "AdvancedHMI v3.99n"
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents DF1ComWF1 As AdvancedHMIDrivers.SerialDF1forSLCMicroCom
    Friend WithEvents ForceItemsIntoToolBox1 As MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox
    Friend WithEvents ModbusTCPCom1 As AdvancedHMIDrivers.ModbusTCPCom
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents PilotLight3Color7 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight1 As AdvancedHMIControls.PilotLight
    Friend WithEvents BasicLabel1 As AdvancedHMIControls.BasicLabel
    Friend WithEvents BasicLabel2 As AdvancedHMIControls.BasicLabel
    Friend WithEvents BasicLabel3 As AdvancedHMIControls.BasicLabel
    Friend WithEvents BasicLabel4 As AdvancedHMIControls.BasicLabel
    Friend WithEvents SelectorSwitch1 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents SelectorSwitch2 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents SelectorSwitch3 As AdvancedHMIControls.SelectorSwitch
    Friend WithEvents PilotLight3Color1 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color2 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color3 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color4 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color5 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents Odometer1 As AdvancedHMIControls.Odometer
End Class
