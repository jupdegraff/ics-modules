﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '   <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    ' <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ForceItemsIntoToolBox1 = New MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox()
        Me.ModbusTCPCom1 = New AdvancedHMIDrivers.ModbusTCPCom(Me.components)
        Me.PilotLight3Color7 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color8 = New AdvancedHMIControls.PilotLight3Color()
        Me.DigitalPanelMeter1 = New AdvancedHMIControls.DigitalPanelMeter()
        Me.BasicLabel1 = New AdvancedHMIControls.BasicLabel()
        Me.PilotLight3Color1 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color2 = New AdvancedHMIControls.PilotLight3Color()
        Me.DigitalPanelMeter2 = New AdvancedHMIControls.DigitalPanelMeter()
        Me.PilotLight3Color3 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color4 = New AdvancedHMIControls.PilotLight3Color()
        Me.DigitalPanelMeter3 = New AdvancedHMIControls.DigitalPanelMeter()
        Me.PilotLight3Color5 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color6 = New AdvancedHMIControls.PilotLight3Color()
        Me.DigitalPanelMeter4 = New AdvancedHMIControls.DigitalPanelMeter()
        Me.SquareIlluminatedButton1 = New AdvancedHMIControls.SquareIlluminatedButton()
        Me.SquareIlluminatedButton2 = New AdvancedHMIControls.SquareIlluminatedButton()
        Me.SquareIlluminatedButton3 = New AdvancedHMIControls.SquareIlluminatedButton()
        Me.SquareIlluminatedButton4 = New AdvancedHMIControls.SquareIlluminatedButton()
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'ModbusTCPCom1
        '
        Me.ModbusTCPCom1.DisableSubscriptions = false
        Me.ModbusTCPCom1.IniFileName = ""
        Me.ModbusTCPCom1.IniFileSection = Nothing
        Me.ModbusTCPCom1.IPAddress = "127.0.0.1"
        Me.ModbusTCPCom1.MaxReadGroupSize = 20
        Me.ModbusTCPCom1.SwapBytes = true
        Me.ModbusTCPCom1.SwapWords = false
        Me.ModbusTCPCom1.TcpipPort = CType(502US,UShort)
        Me.ModbusTCPCom1.TimeOut = 3000
        Me.ModbusTCPCom1.UnitId = CType(1,Byte)
        '
        'PilotLight3Color7
        '
        Me.PilotLight3Color7.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color7.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color7.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color7.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color7.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color7.Location = New System.Drawing.Point(235, 122)
        Me.PilotLight3Color7.Name = "PilotLight3Color7"
        Me.PilotLight3Color7.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color7.PLCaddressClick = ""
        Me.PilotLight3Color7.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color7.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color7.PLCaddressText = ""
        Me.PilotLight3Color7.PLCaddressVisible = ""
        Me.PilotLight3Color7.SelectColor2 = true
        Me.PilotLight3Color7.SelectColor3 = true
        Me.PilotLight3Color7.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color7.TabIndex = 67
        '
        'PilotLight3Color8
        '
        Me.PilotLight3Color8.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color8.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color8.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color8.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color8.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color8.Location = New System.Drawing.Point(194, 122)
        Me.PilotLight3Color8.Name = "PilotLight3Color8"
        Me.PilotLight3Color8.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color8.PLCaddressClick = ""
        Me.PilotLight3Color8.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color8.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color8.PLCaddressText = ""
        Me.PilotLight3Color8.PLCaddressVisible = ""
        Me.PilotLight3Color8.SelectColor2 = true
        Me.PilotLight3Color8.SelectColor3 = true
        Me.PilotLight3Color8.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color8.TabIndex = 66
        '
        'DigitalPanelMeter1
        '
        Me.DigitalPanelMeter1.BackColor = System.Drawing.Color.Transparent
        Me.DigitalPanelMeter1.ComComponent = Me.ModbusTCPCom1
        Me.DigitalPanelMeter1.DecimalPosition = 0
        Me.DigitalPanelMeter1.ForeColor = System.Drawing.Color.LightGray
        Me.DigitalPanelMeter1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.DigitalPanelMeter1.KeypadMaxValue = 0R
        Me.DigitalPanelMeter1.KeypadMinValue = 0R
        Me.DigitalPanelMeter1.KeypadScaleFactor = 1R
        Me.DigitalPanelMeter1.KeypadText = Nothing
        Me.DigitalPanelMeter1.KeypadWidth = 300
        Me.DigitalPanelMeter1.Location = New System.Drawing.Point(38, 100)
        Me.DigitalPanelMeter1.Name = "DigitalPanelMeter1"
        Me.DigitalPanelMeter1.NumberOfDigits = 5
        Me.DigitalPanelMeter1.PLCAddressKeypad = ""
        Me.DigitalPanelMeter1.Resolution = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter1.Size = New System.Drawing.Size(136, 59)
        Me.DigitalPanelMeter1.TabIndex = 58
        Me.DigitalPanelMeter1.Value = 0R
        Me.DigitalPanelMeter1.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter1.ValueScaleOffset = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'BasicLabel1
        '
        Me.BasicLabel1.AutoSize = true
        Me.BasicLabel1.BackColor = System.Drawing.Color.Black
        Me.BasicLabel1.BooleanDisplay = AdvancedHMIControls.BasicLabel.BooleanDisplayOption.TrueFalse
        Me.BasicLabel1.ComComponent = Me.ModbusTCPCom1
        Me.BasicLabel1.DisplayAsTime = false
        Me.BasicLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.BasicLabel1.ForeColor = System.Drawing.Color.White
        Me.BasicLabel1.Highlight = false
        Me.BasicLabel1.HighlightColor = System.Drawing.Color.Red
        Me.BasicLabel1.HighlightForeColor = System.Drawing.Color.White
        Me.BasicLabel1.HighlightKeyCharacter = "!"
        Me.BasicLabel1.KeypadAlpahNumeric = false
        Me.BasicLabel1.KeypadFont = New System.Drawing.Font("Arial", 10!)
        Me.BasicLabel1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.BasicLabel1.KeypadMaxValue = 0R
        Me.BasicLabel1.KeypadMinValue = 0R
        Me.BasicLabel1.KeypadScaleFactor = 1R
        Me.BasicLabel1.KeypadShowCurrentValue = false
        Me.BasicLabel1.KeypadText = Nothing
        Me.BasicLabel1.KeypadWidth = 300
        Me.BasicLabel1.Location = New System.Drawing.Point(27, 26)
        Me.BasicLabel1.Name = "BasicLabel1"
        Me.BasicLabel1.NumericFormat = Nothing
        Me.BasicLabel1.PLCAddressKeypad = ""
        Me.BasicLabel1.PollRate = 0
        Me.BasicLabel1.Size = New System.Drawing.Size(147, 26)
        Me.BasicLabel1.TabIndex = 74
        Me.BasicLabel1.Text = "EMS Control"
        Me.BasicLabel1.Value = "EMS Control"
        Me.BasicLabel1.ValueLeftPadCharacter = Global.Microsoft.VisualBasic.ChrW(32)
        Me.BasicLabel1.ValueLeftPadLength = 0
        Me.BasicLabel1.ValuePrefix = Nothing
        Me.BasicLabel1.ValueScaleFactor = 1R
        Me.BasicLabel1.ValueSuffix = Nothing
        '
        'PilotLight3Color1
        '
        Me.PilotLight3Color1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color1.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color1.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color1.Location = New System.Drawing.Point(235, 236)
        Me.PilotLight3Color1.Name = "PilotLight3Color1"
        Me.PilotLight3Color1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color1.PLCaddressClick = ""
        Me.PilotLight3Color1.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color1.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color1.PLCaddressText = ""
        Me.PilotLight3Color1.PLCaddressVisible = ""
        Me.PilotLight3Color1.SelectColor2 = true
        Me.PilotLight3Color1.SelectColor3 = true
        Me.PilotLight3Color1.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color1.TabIndex = 77
        '
        'PilotLight3Color2
        '
        Me.PilotLight3Color2.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color2.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color2.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color2.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color2.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color2.Location = New System.Drawing.Point(194, 236)
        Me.PilotLight3Color2.Name = "PilotLight3Color2"
        Me.PilotLight3Color2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color2.PLCaddressClick = ""
        Me.PilotLight3Color2.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color2.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color2.PLCaddressText = ""
        Me.PilotLight3Color2.PLCaddressVisible = ""
        Me.PilotLight3Color2.SelectColor2 = true
        Me.PilotLight3Color2.SelectColor3 = true
        Me.PilotLight3Color2.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color2.TabIndex = 76
        '
        'DigitalPanelMeter2
        '
        Me.DigitalPanelMeter2.BackColor = System.Drawing.Color.Transparent
        Me.DigitalPanelMeter2.ComComponent = Me.ModbusTCPCom1
        Me.DigitalPanelMeter2.DecimalPosition = 0
        Me.DigitalPanelMeter2.ForeColor = System.Drawing.Color.LightGray
        Me.DigitalPanelMeter2.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.DigitalPanelMeter2.KeypadMaxValue = 0R
        Me.DigitalPanelMeter2.KeypadMinValue = 0R
        Me.DigitalPanelMeter2.KeypadScaleFactor = 1R
        Me.DigitalPanelMeter2.KeypadText = Nothing
        Me.DigitalPanelMeter2.KeypadWidth = 300
        Me.DigitalPanelMeter2.Location = New System.Drawing.Point(38, 214)
        Me.DigitalPanelMeter2.Name = "DigitalPanelMeter2"
        Me.DigitalPanelMeter2.NumberOfDigits = 5
        Me.DigitalPanelMeter2.PLCAddressKeypad = ""
        Me.DigitalPanelMeter2.Resolution = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter2.Size = New System.Drawing.Size(136, 59)
        Me.DigitalPanelMeter2.TabIndex = 75
        Me.DigitalPanelMeter2.Value = 0R
        Me.DigitalPanelMeter2.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter2.ValueScaleOffset = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'PilotLight3Color3
        '
        Me.PilotLight3Color3.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color3.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color3.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color3.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color3.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color3.Location = New System.Drawing.Point(235, 465)
        Me.PilotLight3Color3.Name = "PilotLight3Color3"
        Me.PilotLight3Color3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color3.PLCaddressClick = ""
        Me.PilotLight3Color3.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color3.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color3.PLCaddressText = ""
        Me.PilotLight3Color3.PLCaddressVisible = ""
        Me.PilotLight3Color3.SelectColor2 = true
        Me.PilotLight3Color3.SelectColor3 = true
        Me.PilotLight3Color3.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color3.TabIndex = 83
        '
        'PilotLight3Color4
        '
        Me.PilotLight3Color4.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color4.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color4.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color4.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color4.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color4.Location = New System.Drawing.Point(194, 465)
        Me.PilotLight3Color4.Name = "PilotLight3Color4"
        Me.PilotLight3Color4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color4.PLCaddressClick = ""
        Me.PilotLight3Color4.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color4.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color4.PLCaddressText = ""
        Me.PilotLight3Color4.PLCaddressVisible = ""
        Me.PilotLight3Color4.SelectColor2 = true
        Me.PilotLight3Color4.SelectColor3 = true
        Me.PilotLight3Color4.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color4.TabIndex = 82
        '
        'DigitalPanelMeter3
        '
        Me.DigitalPanelMeter3.BackColor = System.Drawing.Color.Transparent
        Me.DigitalPanelMeter3.ComComponent = Me.ModbusTCPCom1
        Me.DigitalPanelMeter3.DecimalPosition = 0
        Me.DigitalPanelMeter3.ForeColor = System.Drawing.Color.LightGray
        Me.DigitalPanelMeter3.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.DigitalPanelMeter3.KeypadMaxValue = 0R
        Me.DigitalPanelMeter3.KeypadMinValue = 0R
        Me.DigitalPanelMeter3.KeypadScaleFactor = 1R
        Me.DigitalPanelMeter3.KeypadText = Nothing
        Me.DigitalPanelMeter3.KeypadWidth = 300
        Me.DigitalPanelMeter3.Location = New System.Drawing.Point(38, 443)
        Me.DigitalPanelMeter3.Name = "DigitalPanelMeter3"
        Me.DigitalPanelMeter3.NumberOfDigits = 5
        Me.DigitalPanelMeter3.PLCAddressKeypad = ""
        Me.DigitalPanelMeter3.Resolution = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter3.Size = New System.Drawing.Size(136, 59)
        Me.DigitalPanelMeter3.TabIndex = 81
        Me.DigitalPanelMeter3.Value = 0R
        Me.DigitalPanelMeter3.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter3.ValueScaleOffset = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'PilotLight3Color5
        '
        Me.PilotLight3Color5.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color5.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color5.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color5.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color5.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color5.Location = New System.Drawing.Point(235, 351)
        Me.PilotLight3Color5.Name = "PilotLight3Color5"
        Me.PilotLight3Color5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color5.PLCaddressClick = ""
        Me.PilotLight3Color5.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color5.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color5.PLCaddressText = ""
        Me.PilotLight3Color5.PLCaddressVisible = ""
        Me.PilotLight3Color5.SelectColor2 = true
        Me.PilotLight3Color5.SelectColor3 = true
        Me.PilotLight3Color5.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color5.TabIndex = 80
        '
        'PilotLight3Color6
        '
        Me.PilotLight3Color6.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color6.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color6.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color6.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color6.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color6.Location = New System.Drawing.Point(194, 351)
        Me.PilotLight3Color6.Name = "PilotLight3Color6"
        Me.PilotLight3Color6.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color6.PLCaddressClick = ""
        Me.PilotLight3Color6.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color6.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color6.PLCaddressText = ""
        Me.PilotLight3Color6.PLCaddressVisible = ""
        Me.PilotLight3Color6.SelectColor2 = true
        Me.PilotLight3Color6.SelectColor3 = true
        Me.PilotLight3Color6.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color6.TabIndex = 79
        '
        'DigitalPanelMeter4
        '
        Me.DigitalPanelMeter4.BackColor = System.Drawing.Color.Transparent
        Me.DigitalPanelMeter4.ComComponent = Me.ModbusTCPCom1
        Me.DigitalPanelMeter4.DecimalPosition = 0
        Me.DigitalPanelMeter4.ForeColor = System.Drawing.Color.LightGray
        Me.DigitalPanelMeter4.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.DigitalPanelMeter4.KeypadMaxValue = 0R
        Me.DigitalPanelMeter4.KeypadMinValue = 0R
        Me.DigitalPanelMeter4.KeypadScaleFactor = 1R
        Me.DigitalPanelMeter4.KeypadText = Nothing
        Me.DigitalPanelMeter4.KeypadWidth = 300
        Me.DigitalPanelMeter4.Location = New System.Drawing.Point(38, 329)
        Me.DigitalPanelMeter4.Name = "DigitalPanelMeter4"
        Me.DigitalPanelMeter4.NumberOfDigits = 5
        Me.DigitalPanelMeter4.PLCAddressKeypad = ""
        Me.DigitalPanelMeter4.Resolution = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter4.Size = New System.Drawing.Size(136, 59)
        Me.DigitalPanelMeter4.TabIndex = 78
        Me.DigitalPanelMeter4.Value = 0R
        Me.DigitalPanelMeter4.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter4.ValueScaleOffset = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'SquareIlluminatedButton1
        '
        Me.SquareIlluminatedButton1.ComComponent = Me.ModbusTCPCom1
        Me.SquareIlluminatedButton1.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.SquareIlluminatedButton1.Location = New System.Drawing.Point(297, 136)
        Me.SquareIlluminatedButton1.Name = "SquareIlluminatedButton1"
        Me.SquareIlluminatedButton1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SquareIlluminatedButton1.PLCAddressClick = ""
        Me.SquareIlluminatedButton1.PLCAddressText = ""
        Me.SquareIlluminatedButton1.PLCAddressValue = ""
        Me.SquareIlluminatedButton1.PLCAddressVisible = ""
        Me.SquareIlluminatedButton1.Size = New System.Drawing.Size(75, 23)
        Me.SquareIlluminatedButton1.TabIndex = 84
        Me.SquareIlluminatedButton1.Text = "Connected"
        Me.SquareIlluminatedButton1.Value = false
        '
        'SquareIlluminatedButton2
        '
        Me.SquareIlluminatedButton2.ComComponent = Me.ModbusTCPCom1
        Me.SquareIlluminatedButton2.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.SquareIlluminatedButton2.Location = New System.Drawing.Point(297, 250)
        Me.SquareIlluminatedButton2.Name = "SquareIlluminatedButton2"
        Me.SquareIlluminatedButton2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SquareIlluminatedButton2.PLCAddressClick = ""
        Me.SquareIlluminatedButton2.PLCAddressText = ""
        Me.SquareIlluminatedButton2.PLCAddressValue = ""
        Me.SquareIlluminatedButton2.PLCAddressVisible = ""
        Me.SquareIlluminatedButton2.Size = New System.Drawing.Size(75, 23)
        Me.SquareIlluminatedButton2.TabIndex = 85
        Me.SquareIlluminatedButton2.Text = "Connected"
        Me.SquareIlluminatedButton2.Value = false
        '
        'SquareIlluminatedButton3
        '
        Me.SquareIlluminatedButton3.ComComponent = Me.ModbusTCPCom1
        Me.SquareIlluminatedButton3.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.SquareIlluminatedButton3.Location = New System.Drawing.Point(297, 365)
        Me.SquareIlluminatedButton3.Name = "SquareIlluminatedButton3"
        Me.SquareIlluminatedButton3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SquareIlluminatedButton3.PLCAddressClick = ""
        Me.SquareIlluminatedButton3.PLCAddressText = ""
        Me.SquareIlluminatedButton3.PLCAddressValue = ""
        Me.SquareIlluminatedButton3.PLCAddressVisible = ""
        Me.SquareIlluminatedButton3.Size = New System.Drawing.Size(75, 23)
        Me.SquareIlluminatedButton3.TabIndex = 86
        Me.SquareIlluminatedButton3.Text = "Connected"
        Me.SquareIlluminatedButton3.Value = false
        '
        'SquareIlluminatedButton4
        '
        Me.SquareIlluminatedButton4.ComComponent = Me.ModbusTCPCom1
        Me.SquareIlluminatedButton4.LightColor = MfgControl.AdvancedHMI.Controls.SquareIlluminatedButton.LightColors.Green
        Me.SquareIlluminatedButton4.Location = New System.Drawing.Point(297, 479)
        Me.SquareIlluminatedButton4.Name = "SquareIlluminatedButton4"
        Me.SquareIlluminatedButton4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.SquareIlluminatedButton4.PLCAddressClick = ""
        Me.SquareIlluminatedButton4.PLCAddressText = ""
        Me.SquareIlluminatedButton4.PLCAddressValue = ""
        Me.SquareIlluminatedButton4.PLCAddressVisible = ""
        Me.SquareIlluminatedButton4.Size = New System.Drawing.Size(75, 23)
        Me.SquareIlluminatedButton4.TabIndex = 87
        Me.SquareIlluminatedButton4.Text = "Connected"
        Me.SquareIlluminatedButton4.Value = false
        '
        'MainForm
        '
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.SquareIlluminatedButton4)
        Me.Controls.Add(Me.SquareIlluminatedButton3)
        Me.Controls.Add(Me.SquareIlluminatedButton2)
        Me.Controls.Add(Me.SquareIlluminatedButton1)
        Me.Controls.Add(Me.PilotLight3Color3)
        Me.Controls.Add(Me.PilotLight3Color4)
        Me.Controls.Add(Me.DigitalPanelMeter3)
        Me.Controls.Add(Me.PilotLight3Color5)
        Me.Controls.Add(Me.PilotLight3Color6)
        Me.Controls.Add(Me.DigitalPanelMeter4)
        Me.Controls.Add(Me.PilotLight3Color1)
        Me.Controls.Add(Me.PilotLight3Color2)
        Me.Controls.Add(Me.DigitalPanelMeter2)
        Me.Controls.Add(Me.BasicLabel1)
        Me.Controls.Add(Me.PilotLight3Color7)
        Me.Controls.Add(Me.PilotLight3Color8)
        Me.Controls.Add(Me.DigitalPanelMeter1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.KeyPreview = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "AdvancedHMI v3.99n"
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents DF1ComWF1 As AdvancedHMIDrivers.SerialDF1forSLCMicroCom
    Friend WithEvents ForceItemsIntoToolBox1 As MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox
    Friend WithEvents ModbusTCPCom1 As AdvancedHMIDrivers.ModbusTCPCom
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents DigitalPanelMeter1 As AdvancedHMIControls.DigitalPanelMeter
    Friend WithEvents PilotLight3Color7 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color8 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents BasicLabel1 As AdvancedHMIControls.BasicLabel
    Friend WithEvents PilotLight3Color1 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color2 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents DigitalPanelMeter2 As AdvancedHMIControls.DigitalPanelMeter
    Friend WithEvents PilotLight3Color3 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color4 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents DigitalPanelMeter3 As AdvancedHMIControls.DigitalPanelMeter
    Friend WithEvents PilotLight3Color5 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color6 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents DigitalPanelMeter4 As AdvancedHMIControls.DigitalPanelMeter
    Friend WithEvents SquareIlluminatedButton1 As AdvancedHMIControls.SquareIlluminatedButton
    Friend WithEvents SquareIlluminatedButton2 As AdvancedHMIControls.SquareIlluminatedButton
    Friend WithEvents SquareIlluminatedButton3 As AdvancedHMIControls.SquareIlluminatedButton
    Friend WithEvents SquareIlluminatedButton4 As AdvancedHMIControls.SquareIlluminatedButton
End Class
