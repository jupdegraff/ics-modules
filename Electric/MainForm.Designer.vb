﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    '   <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    ' <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ForceItemsIntoToolBox1 = New MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox()
        Me.ModbusTCPCom1 = New AdvancedHMIDrivers.ModbusTCPCom(Me.components)
        Me.PilotLight1 = New AdvancedHMIControls.PilotLight()
        Me.SelectorSwitch3Pos4 = New AdvancedHMIControls.SelectorSwitch3Pos()
        Me.SelectorSwitch3Pos3 = New AdvancedHMIControls.SelectorSwitch3Pos()
        Me.SelectorSwitch3Pos2 = New AdvancedHMIControls.SelectorSwitch3Pos()
        Me.SelectorSwitch3Pos1 = New AdvancedHMIControls.SelectorSwitch3Pos()
        Me.ThreeButtons1 = New AdvancedHMIControls.ThreeButtons()
        Me.PilotLight3Color7 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color8 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color5 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color6 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color2 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color4 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color3 = New AdvancedHMIControls.PilotLight3Color()
        Me.PilotLight3Color1 = New AdvancedHMIControls.PilotLight3Color()
        Me.DigitalPanelMeter1 = New AdvancedHMIControls.DigitalPanelMeter()
        Me.Gauge4 = New AdvancedHMIControls.Gauge()
        Me.Gauge3 = New AdvancedHMIControls.Gauge()
        Me.Gauge2 = New AdvancedHMIControls.Gauge()
        Me.Gauge1 = New AdvancedHMIControls.Gauge()
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'ModbusTCPCom1
        '
        Me.ModbusTCPCom1.DisableSubscriptions = false
        Me.ModbusTCPCom1.IniFileName = ""
        Me.ModbusTCPCom1.IniFileSection = Nothing
        Me.ModbusTCPCom1.IPAddress = "127.0.0.1"
        Me.ModbusTCPCom1.MaxReadGroupSize = 20
        Me.ModbusTCPCom1.SwapBytes = true
        Me.ModbusTCPCom1.SwapWords = false
        Me.ModbusTCPCom1.TcpipPort = CType(502US,UShort)
        Me.ModbusTCPCom1.TimeOut = 3000
        Me.ModbusTCPCom1.UnitId = CType(1,Byte)
        '
        'PilotLight1
        '
        Me.PilotLight1.Blink = false
        Me.PilotLight1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight.LegendPlates.Large
        Me.PilotLight1.LightColor = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight.LightColors.Red
        Me.PilotLight1.Location = New System.Drawing.Point(679, 430)
        Me.PilotLight1.Name = "PilotLight1"
        Me.PilotLight1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight1.PLCAddressClick = ""
        Me.PilotLight1.PLCAddressText = ""
        Me.PilotLight1.PLCAddressValue = ""
        Me.PilotLight1.PLCAddressVisible = ""
        Me.PilotLight1.Size = New System.Drawing.Size(75, 110)
        Me.PilotLight1.TabIndex = 73
        Me.PilotLight1.Text = "Emergency Stop"
        Me.PilotLight1.Value = false
        '
        'SelectorSwitch3Pos4
        '
        Me.SelectorSwitch3Pos4.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3Pos4.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch3Pos.LegendPlates.Small
        Me.SelectorSwitch3Pos4.Location = New System.Drawing.Point(508, 346)
        Me.SelectorSwitch3Pos4.Name = "SelectorSwitch3Pos4"
        Me.SelectorSwitch3Pos4.PLCAddressClickLeft = ""
        Me.SelectorSwitch3Pos4.PLCAddressClickRight = ""
        Me.SelectorSwitch3Pos4.PLCAddressText = ""
        Me.SelectorSwitch3Pos4.PLCAddressValueLeft = ""
        Me.SelectorSwitch3Pos4.PLCAddressValueRight = ""
        Me.SelectorSwitch3Pos4.PLCAddressVisible = ""
        Me.SelectorSwitch3Pos4.Size = New System.Drawing.Size(60, 88)
        Me.SelectorSwitch3Pos4.TabIndex = 72
        Me.SelectorSwitch3Pos4.Value = 1
        Me.SelectorSwitch3Pos4.ValueLeft = false
        Me.SelectorSwitch3Pos4.ValueOfCenterPosition = 0
        Me.SelectorSwitch3Pos4.ValueOfLeftPosition = -1
        Me.SelectorSwitch3Pos4.ValueOfRightPosition = 1
        Me.SelectorSwitch3Pos4.ValueRight = true
        '
        'SelectorSwitch3Pos3
        '
        Me.SelectorSwitch3Pos3.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3Pos3.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch3Pos.LegendPlates.Small
        Me.SelectorSwitch3Pos3.Location = New System.Drawing.Point(364, 346)
        Me.SelectorSwitch3Pos3.Name = "SelectorSwitch3Pos3"
        Me.SelectorSwitch3Pos3.PLCAddressClickLeft = ""
        Me.SelectorSwitch3Pos3.PLCAddressClickRight = ""
        Me.SelectorSwitch3Pos3.PLCAddressText = ""
        Me.SelectorSwitch3Pos3.PLCAddressValueLeft = ""
        Me.SelectorSwitch3Pos3.PLCAddressValueRight = ""
        Me.SelectorSwitch3Pos3.PLCAddressVisible = ""
        Me.SelectorSwitch3Pos3.Size = New System.Drawing.Size(60, 88)
        Me.SelectorSwitch3Pos3.TabIndex = 71
        Me.SelectorSwitch3Pos3.Value = 1
        Me.SelectorSwitch3Pos3.ValueLeft = false
        Me.SelectorSwitch3Pos3.ValueOfCenterPosition = 0
        Me.SelectorSwitch3Pos3.ValueOfLeftPosition = -1
        Me.SelectorSwitch3Pos3.ValueOfRightPosition = 1
        Me.SelectorSwitch3Pos3.ValueRight = true
        '
        'SelectorSwitch3Pos2
        '
        Me.SelectorSwitch3Pos2.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3Pos2.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch3Pos.LegendPlates.Small
        Me.SelectorSwitch3Pos2.Location = New System.Drawing.Point(222, 346)
        Me.SelectorSwitch3Pos2.Name = "SelectorSwitch3Pos2"
        Me.SelectorSwitch3Pos2.PLCAddressClickLeft = "40002"
        Me.SelectorSwitch3Pos2.PLCAddressClickRight = "40002"
        Me.SelectorSwitch3Pos2.PLCAddressText = ""
        Me.SelectorSwitch3Pos2.PLCAddressValueLeft = ""
        Me.SelectorSwitch3Pos2.PLCAddressValueRight = ""
        Me.SelectorSwitch3Pos2.PLCAddressVisible = ""
        Me.SelectorSwitch3Pos2.Size = New System.Drawing.Size(60, 88)
        Me.SelectorSwitch3Pos2.TabIndex = 70
        Me.SelectorSwitch3Pos2.Value = 1
        Me.SelectorSwitch3Pos2.ValueLeft = false
        Me.SelectorSwitch3Pos2.ValueOfCenterPosition = 0
        Me.SelectorSwitch3Pos2.ValueOfLeftPosition = -1
        Me.SelectorSwitch3Pos2.ValueOfRightPosition = 1
        Me.SelectorSwitch3Pos2.ValueRight = true
        '
        'SelectorSwitch3Pos1
        '
        Me.SelectorSwitch3Pos1.ComComponent = Me.ModbusTCPCom1
        Me.SelectorSwitch3Pos1.LegendPlate = MfgControl.AdvancedHMI.Controls.SelectorSwitch3Pos.LegendPlates.Small
        Me.SelectorSwitch3Pos1.Location = New System.Drawing.Point(78, 346)
        Me.SelectorSwitch3Pos1.Name = "SelectorSwitch3Pos1"
        Me.SelectorSwitch3Pos1.PLCAddressClickLeft = "40001"
        Me.SelectorSwitch3Pos1.PLCAddressClickRight = "40001"
        Me.SelectorSwitch3Pos1.PLCAddressText = ""
        Me.SelectorSwitch3Pos1.PLCAddressValueLeft = ""
        Me.SelectorSwitch3Pos1.PLCAddressValueRight = ""
        Me.SelectorSwitch3Pos1.PLCAddressVisible = ""
        Me.SelectorSwitch3Pos1.Size = New System.Drawing.Size(60, 88)
        Me.SelectorSwitch3Pos1.TabIndex = 69
        Me.SelectorSwitch3Pos1.Value = 50
        Me.SelectorSwitch3Pos1.ValueLeft = false
        Me.SelectorSwitch3Pos1.ValueOfCenterPosition = 50
        Me.SelectorSwitch3Pos1.ValueOfLeftPosition = 0
        Me.SelectorSwitch3Pos1.ValueOfRightPosition = 100
        Me.SelectorSwitch3Pos1.ValueRight = false
        '
        'ThreeButtons1
        '
        Me.ThreeButtons1.Button1Text = "Reset"
        Me.ThreeButtons1.Button2Text = "On"
        Me.ThreeButtons1.Button3Text = "Off"
        Me.ThreeButtons1.Location = New System.Drawing.Point(618, 250)
        Me.ThreeButtons1.Name = "ThreeButtons1"
        Me.ThreeButtons1.Size = New System.Drawing.Size(136, 161)
        Me.ThreeButtons1.TabIndex = 68
        '
        'PilotLight3Color7
        '
        Me.PilotLight3Color7.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color7.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color7.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color7.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color7.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color7.Location = New System.Drawing.Point(533, 135)
        Me.PilotLight3Color7.Name = "PilotLight3Color7"
        Me.PilotLight3Color7.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color7.PLCaddressClick = ""
        Me.PilotLight3Color7.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color7.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color7.PLCaddressText = ""
        Me.PilotLight3Color7.PLCaddressVisible = ""
        Me.PilotLight3Color7.SelectColor2 = true
        Me.PilotLight3Color7.SelectColor3 = true
        Me.PilotLight3Color7.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color7.TabIndex = 67
        '
        'PilotLight3Color8
        '
        Me.PilotLight3Color8.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color8.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color8.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color8.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color8.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color8.Location = New System.Drawing.Point(492, 135)
        Me.PilotLight3Color8.Name = "PilotLight3Color8"
        Me.PilotLight3Color8.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color8.PLCaddressClick = ""
        Me.PilotLight3Color8.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color8.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color8.PLCaddressText = ""
        Me.PilotLight3Color8.PLCaddressVisible = ""
        Me.PilotLight3Color8.SelectColor2 = true
        Me.PilotLight3Color8.SelectColor3 = true
        Me.PilotLight3Color8.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color8.TabIndex = 66
        '
        'PilotLight3Color5
        '
        Me.PilotLight3Color5.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color5.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color5.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color5.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color5.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color5.Location = New System.Drawing.Point(389, 135)
        Me.PilotLight3Color5.Name = "PilotLight3Color5"
        Me.PilotLight3Color5.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color5.PLCaddressClick = ""
        Me.PilotLight3Color5.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color5.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color5.PLCaddressText = ""
        Me.PilotLight3Color5.PLCaddressVisible = ""
        Me.PilotLight3Color5.SelectColor2 = true
        Me.PilotLight3Color5.SelectColor3 = true
        Me.PilotLight3Color5.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color5.TabIndex = 65
        '
        'PilotLight3Color6
        '
        Me.PilotLight3Color6.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color6.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color6.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color6.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color6.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color6.Location = New System.Drawing.Point(348, 135)
        Me.PilotLight3Color6.Name = "PilotLight3Color6"
        Me.PilotLight3Color6.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color6.PLCaddressClick = ""
        Me.PilotLight3Color6.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color6.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color6.PLCaddressText = ""
        Me.PilotLight3Color6.PLCaddressVisible = ""
        Me.PilotLight3Color6.SelectColor2 = true
        Me.PilotLight3Color6.SelectColor3 = true
        Me.PilotLight3Color6.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color6.TabIndex = 64
        '
        'PilotLight3Color2
        '
        Me.PilotLight3Color2.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color2.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color2.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color2.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color2.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color2.Location = New System.Drawing.Point(247, 135)
        Me.PilotLight3Color2.Name = "PilotLight3Color2"
        Me.PilotLight3Color2.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color2.PLCaddressClick = ""
        Me.PilotLight3Color2.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color2.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color2.PLCaddressText = ""
        Me.PilotLight3Color2.PLCaddressVisible = ""
        Me.PilotLight3Color2.SelectColor2 = true
        Me.PilotLight3Color2.SelectColor3 = true
        Me.PilotLight3Color2.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color2.TabIndex = 63
        '
        'PilotLight3Color4
        '
        Me.PilotLight3Color4.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color4.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color4.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color4.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color4.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color4.Location = New System.Drawing.Point(206, 135)
        Me.PilotLight3Color4.Name = "PilotLight3Color4"
        Me.PilotLight3Color4.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color4.PLCaddressClick = ""
        Me.PilotLight3Color4.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color4.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color4.PLCaddressText = ""
        Me.PilotLight3Color4.PLCaddressVisible = ""
        Me.PilotLight3Color4.SelectColor2 = true
        Me.PilotLight3Color4.SelectColor3 = true
        Me.PilotLight3Color4.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color4.TabIndex = 62
        '
        'PilotLight3Color3
        '
        Me.PilotLight3Color3.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color3.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color3.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color3.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color3.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color3.Location = New System.Drawing.Point(103, 135)
        Me.PilotLight3Color3.Name = "PilotLight3Color3"
        Me.PilotLight3Color3.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.MomentarySet
        Me.PilotLight3Color3.PLCaddressClick = ""
        Me.PilotLight3Color3.PLCaddressSelectColor2 = ""
        Me.PilotLight3Color3.PLCaddressSelectColor3 = ""
        Me.PilotLight3Color3.PLCaddressText = ""
        Me.PilotLight3Color3.PLCaddressVisible = ""
        Me.PilotLight3Color3.SelectColor2 = true
        Me.PilotLight3Color3.SelectColor3 = true
        Me.PilotLight3Color3.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color3.TabIndex = 61
        '
        'PilotLight3Color1
        '
        Me.PilotLight3Color1.ComComponent = Me.ModbusTCPCom1
        Me.PilotLight3Color1.LegendPlate = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LegendPlates.Small
        Me.PilotLight3Color1.LightColor1 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.White
        Me.PilotLight3Color1.LightColor3 = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Red
        Me.PilotLight3Color1.LightColorOff = MfgControl.AdvancedHMI.Controls.PilotLight3Color.LightColors.Green
        Me.PilotLight3Color1.Location = New System.Drawing.Point(62, 135)
        Me.PilotLight3Color1.Name = "PilotLight3Color1"
        Me.PilotLight3Color1.OutputType = MfgControl.AdvancedHMI.Controls.OutputType.WriteValue
        Me.PilotLight3Color1.PLCaddressClick = "40001"
        Me.PilotLight3Color1.PLCaddressSelectColor2 = "40001"
        Me.PilotLight3Color1.PLCaddressSelectColor3 = "40001"
        Me.PilotLight3Color1.PLCaddressText = ""
        Me.PilotLight3Color1.PLCaddressVisible = ""
        Me.PilotLight3Color1.SelectColor2 = true
        Me.PilotLight3Color1.SelectColor3 = true
        Me.PilotLight3Color1.Size = New System.Drawing.Size(35, 37)
        Me.PilotLight3Color1.TabIndex = 59
        '
        'DigitalPanelMeter1
        '
        Me.DigitalPanelMeter1.BackColor = System.Drawing.Color.Transparent
        Me.DigitalPanelMeter1.ComComponent = Me.ModbusTCPCom1
        Me.DigitalPanelMeter1.DecimalPosition = 0
        Me.DigitalPanelMeter1.ForeColor = System.Drawing.Color.LightGray
        Me.DigitalPanelMeter1.KeypadFontColor = System.Drawing.Color.WhiteSmoke
        Me.DigitalPanelMeter1.KeypadMaxValue = 0R
        Me.DigitalPanelMeter1.KeypadMinValue = 0R
        Me.DigitalPanelMeter1.KeypadScaleFactor = 1R
        Me.DigitalPanelMeter1.KeypadText = Nothing
        Me.DigitalPanelMeter1.KeypadWidth = 300
        Me.DigitalPanelMeter1.Location = New System.Drawing.Point(618, 181)
        Me.DigitalPanelMeter1.Name = "DigitalPanelMeter1"
        Me.DigitalPanelMeter1.NumberOfDigits = 5
        Me.DigitalPanelMeter1.PLCAddressKeypad = ""
        Me.DigitalPanelMeter1.Resolution = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter1.Size = New System.Drawing.Size(136, 59)
        Me.DigitalPanelMeter1.TabIndex = 58
        Me.DigitalPanelMeter1.Value = 0R
        Me.DigitalPanelMeter1.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        Me.DigitalPanelMeter1.ValueScaleOffset = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'Gauge4
        '
        Me.Gauge4.AllowDragging = false
        Me.Gauge4.BackColor = System.Drawing.Color.Transparent
        Me.Gauge4.ComComponent = Me.ModbusTCPCom1
        Me.Gauge4.HighlightColor = System.Drawing.Color.Red
        Me.Gauge4.Location = New System.Drawing.Point(452, 190)
        Me.Gauge4.MaxValue = 100
        Me.Gauge4.MinValue = 0
        Me.Gauge4.Name = "Gauge4"
        Me.Gauge4.NumericFormat = Nothing
        Me.Gauge4.PLCAddressText = ""
        Me.Gauge4.PLCAddressValue = "40004"
        Me.Gauge4.PLCAddressVisible = ""
        Me.Gauge4.Size = New System.Drawing.Size(137, 137)
        Me.Gauge4.TabIndex = 57
        Me.Gauge4.Value = 0R
        Me.Gauge4.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Gauge3
        '
        Me.Gauge3.AllowDragging = false
        Me.Gauge3.BackColor = System.Drawing.Color.Transparent
        Me.Gauge3.ComComponent = Me.ModbusTCPCom1
        Me.Gauge3.HighlightColor = System.Drawing.Color.Red
        Me.Gauge3.Location = New System.Drawing.Point(309, 190)
        Me.Gauge3.MaxValue = 100
        Me.Gauge3.MinValue = 0
        Me.Gauge3.Name = "Gauge3"
        Me.Gauge3.NumericFormat = Nothing
        Me.Gauge3.PLCAddressText = ""
        Me.Gauge3.PLCAddressValue = "40003"
        Me.Gauge3.PLCAddressVisible = ""
        Me.Gauge3.Size = New System.Drawing.Size(137, 137)
        Me.Gauge3.TabIndex = 56
        Me.Gauge3.Value = 0R
        Me.Gauge3.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Gauge2
        '
        Me.Gauge2.AllowDragging = false
        Me.Gauge2.BackColor = System.Drawing.Color.Transparent
        Me.Gauge2.ComComponent = Me.ModbusTCPCom1
        Me.Gauge2.HighlightColor = System.Drawing.Color.Red
        Me.Gauge2.Location = New System.Drawing.Point(166, 190)
        Me.Gauge2.MaxValue = 100
        Me.Gauge2.MinValue = 0
        Me.Gauge2.Name = "Gauge2"
        Me.Gauge2.NumericFormat = Nothing
        Me.Gauge2.PLCAddressText = ""
        Me.Gauge2.PLCAddressValue = "40002"
        Me.Gauge2.PLCAddressVisible = ""
        Me.Gauge2.Size = New System.Drawing.Size(137, 137)
        Me.Gauge2.TabIndex = 55
        Me.Gauge2.Value = 0R
        Me.Gauge2.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Gauge1
        '
        Me.Gauge1.AllowDragging = false
        Me.Gauge1.BackColor = System.Drawing.Color.Transparent
        Me.Gauge1.ComComponent = Me.ModbusTCPCom1
        Me.Gauge1.HighlightColor = System.Drawing.Color.Red
        Me.Gauge1.Location = New System.Drawing.Point(23, 190)
        Me.Gauge1.MaxValue = 100
        Me.Gauge1.MinValue = 0
        Me.Gauge1.Name = "Gauge1"
        Me.Gauge1.NumericFormat = Nothing
        Me.Gauge1.PLCAddressText = ""
        Me.Gauge1.PLCAddressValue = "40001"
        Me.Gauge1.PLCAddressVisible = ""
        Me.Gauge1.Size = New System.Drawing.Size(137, 137)
        Me.Gauge1.TabIndex = 54
        Me.Gauge1.Value = 0R
        Me.Gauge1.ValueScaleFactor = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'MainForm
        '
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.PilotLight1)
        Me.Controls.Add(Me.SelectorSwitch3Pos4)
        Me.Controls.Add(Me.SelectorSwitch3Pos3)
        Me.Controls.Add(Me.SelectorSwitch3Pos2)
        Me.Controls.Add(Me.SelectorSwitch3Pos1)
        Me.Controls.Add(Me.ThreeButtons1)
        Me.Controls.Add(Me.PilotLight3Color7)
        Me.Controls.Add(Me.PilotLight3Color8)
        Me.Controls.Add(Me.PilotLight3Color5)
        Me.Controls.Add(Me.PilotLight3Color6)
        Me.Controls.Add(Me.PilotLight3Color2)
        Me.Controls.Add(Me.PilotLight3Color4)
        Me.Controls.Add(Me.PilotLight3Color3)
        Me.Controls.Add(Me.PilotLight3Color1)
        Me.Controls.Add(Me.DigitalPanelMeter1)
        Me.Controls.Add(Me.Gauge4)
        Me.Controls.Add(Me.Gauge3)
        Me.Controls.Add(Me.Gauge2)
        Me.Controls.Add(Me.Gauge1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.KeyPreview = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "AdvancedHMI v3.99n"
        CType(Me.ModbusTCPCom1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents DF1ComWF1 As AdvancedHMIDrivers.SerialDF1forSLCMicroCom
    Friend WithEvents ForceItemsIntoToolBox1 As MfgControl.AdvancedHMI.Drivers.ForceItemsIntoToolbox
    Friend WithEvents ModbusTCPCom1 As AdvancedHMIDrivers.ModbusTCPCom
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Gauge1 As AdvancedHMIControls.Gauge
    Friend WithEvents Gauge2 As AdvancedHMIControls.Gauge
    Friend WithEvents Gauge3 As AdvancedHMIControls.Gauge
    Friend WithEvents Gauge4 As AdvancedHMIControls.Gauge
    Friend WithEvents DigitalPanelMeter1 As AdvancedHMIControls.DigitalPanelMeter
    Friend WithEvents PilotLight3Color1 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color3 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color2 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color4 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color5 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color6 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color7 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents PilotLight3Color8 As AdvancedHMIControls.PilotLight3Color
    Friend WithEvents ThreeButtons1 As AdvancedHMIControls.ThreeButtons
    Friend WithEvents SelectorSwitch3Pos1 As AdvancedHMIControls.SelectorSwitch3Pos
    Friend WithEvents SelectorSwitch3Pos2 As AdvancedHMIControls.SelectorSwitch3Pos
    Friend WithEvents SelectorSwitch3Pos3 As AdvancedHMIControls.SelectorSwitch3Pos
    Friend WithEvents SelectorSwitch3Pos4 As AdvancedHMIControls.SelectorSwitch3Pos
    Friend WithEvents PilotLight1 As AdvancedHMIControls.PilotLight
End Class
